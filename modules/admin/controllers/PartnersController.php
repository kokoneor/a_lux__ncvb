<?php

namespace app\modules\admin\controllers;

use app\models\MultilingualActiveRecord;
use Yii;
use app\models\Partners;
use app\models\search\PartnersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\admin\controllers\BackendController;
use yii\web\UploadedFile;

/**
 * PartnersController implements the CRUD actions for Partners model.
 */
class PartnersController extends BackendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Partners models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PartnersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Partners model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Partners model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Partners();

        if ($model->load(Yii::$app->request->post())) {

            $image = UploadedFile::getInstance($model, 'image');
            if ($image !== null) {
                $time = time();
                $image->saveAs($model->path . $time . '_' . $image->baseName . '.' . $image->extension);
                $model->image = $time . '_' . $image->baseName . '.' . $image->extension;
            }

            $image_kk = UploadedFile::getInstance($model, 'image_kk');
            if ($image_kk !== null) {
                $time = time();
                $image_kk->saveAs($model->path . $time . '_' . $image_kk->baseName . '.' . $image_kk->extension);
                $model->image_kk = $time . '_' . $image_kk->baseName . '.' . $image_kk->extension;
            }

            $image_en = UploadedFile::getInstance($model, 'image_en');
            if ($image_en !== null) {
                $time = time();
                $image_en->saveAs($model->path . $time . '_' . $image_en->baseName . '.' . $image_en->extension);
                $model->image_en = $time . '_' . $image_en->baseName . '.' . $image_en->extension;
            }

            if($model->save()) {

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Partners model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldImage       = $model->image;
        $oldImage_kk    = $model->image_kk;
        $oldImage_en    = $model->image_en;

        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            if ($image == null) {
                $model->image = $oldImage;
            } else {
                $time = time();
                $image->saveAs($model->path . $time . '_' . $image->baseName . '.' . $image->extension);
                $model->image = $time . '_' . $image->baseName . '.' . $image->extension;

                if (!($oldImage == null)) {
                    if(file_exists($model->path . $oldImage)){
                        unlink($model->path . $oldImage);
                    }
                }
            }

            $image_kk = UploadedFile::getInstance($model, 'image_kk');
            if ($image_kk == null) {
                $model->image_kk = $oldImage_kk;
            } else {
                $time = time();
                $image_kk->saveAs($model->path . $time . '_' . $image_kk->baseName . '.' . $image_kk->extension);
                $model->image_kk = $time . '_' . $image_kk->baseName . '.' . $image_kk->extension;

                if (!($oldImage_kk == null)) {
                    if(file_exists($model->path . $oldImage_kk)){
                        unlink($model->path . $oldImage_kk);
                    }
                }
            }

            $image_en = UploadedFile::getInstance($model, 'image_en');
            if ($image_en == null) {
                $model->image_en = $oldImage_en;
            } else {
                $time = time();
                $image_kk->saveAs($model->path . $time . '_' . $image_en->baseName . '.' . $image_en->extension);
                $model->image_en = $time . '_' . $image_en->baseName . '.' . $image_en->extension;

                if (!($oldImage_en == null)) {
                    if(file_exists($model->path . $oldImage_en)){
                        unlink($model->path . $oldImage_en);
                    }
                }
            }

            if($model->save()) {

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Partners model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Deletes image an existing Partners model.
     * If deletion is successful, the browser will be redirected to the 'form' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
    */
    public function actionDeleteImage($id)
    {
        $model = $this->findModel($id);

        $this->deleteImage($model);

        if($model->save(false)){
        return $this->redirect(['update?id='.$id]);
        }
    }

    /**
     * Finds the Partners model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /* @var $activeRecord MultilingualActiveRecord */

        $model = Partners::find()->multilingual()->where(['id' => $id])->one();

        if (null === $model) {
            throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }
}
