<?php

namespace app\modules\admin\controllers;

use app\models\MultilingualActiveRecord;
use Yii;
use app\models\Banner;
use app\models\search\BannerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\admin\controllers\BackendController;
use yii\web\UploadedFile;

/**
 * BannerController implements the CRUD actions for Banner model.
 */
class BannerController extends BackendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Banner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BannerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Banner model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Banner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Banner();

        if ($model->load(Yii::$app->request->post())) {
            $this->createImage($model);

            $images = [];
            foreach (\app\models\i18n::locales() as $locale){
                if (Yii::$app->language <> $locale){
                    $image_lang = UploadedFile::getInstance($model, 'image_'.$locale);
                    if ($image_lang !== null) {
                        $time = time();
                        $image_lang->saveAs($model->path . $time . '_' . $image_lang->baseName . '.' . $image_lang->extension);
                        $images[$locale] = $time . '_' . $image_lang->baseName . '.' . $image_lang->extension;
                    }
                }
            }

            if($model->save()) {
                foreach ($model->bannerTranslations as $bannerTranslation){
                    if(isset($images[$bannerTranslation->language])){
                        $bannerTranslation->image   = $images[$bannerTranslation->language];
                        $bannerTranslation->save();
                    }
                }

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Banner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldImage = $model->image;

        if ($model->load(Yii::$app->request->post())) {
            $this->updateImage($model, $oldImage);
            $images = [];
            foreach (\app\models\i18n::locales() as $locale){
                if (Yii::$app->language <> $locale){
                    $image_lang = UploadedFile::getInstance($model, 'image_'.$locale);
                    if ($image_lang == null) {
                    } else {
                        $time = time();
                        $image_lang->saveAs($model->path . $time . '_' . $image_lang->baseName . '.' . $image_lang->extension);
                        $images[$locale] = $time . '_' . $image_lang->baseName . '.' . $image_lang->extension;
                    }
                }
            }

            if($model->save()) {
                foreach ($model->bannerTranslations as $bannerTranslation){
                    if(isset($images[$bannerTranslation->language])){
                        $bannerTranslation->image   = $images[$bannerTranslation->language];
                        $bannerTranslation->save();
                    }
                }

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Banner model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Deletes image an existing Banner model.
     * If deletion is successful, the browser will be redirected to the 'form' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
    */
    public function actionDeleteImage($id)
    {
        $model = $this->findModel($id);

        $this->deleteImage($model);

        if($model->save(false)){
        return $this->redirect(['update?id='.$id]);
        }
    }

    /**
     * Finds the Banner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /* @var $activeRecord MultilingualActiveRecord */

        $model = Banner::find()->multilingual()->where(['id' => $id])->one();

        if (null === $model) {
            throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }

    public function actionMoveUp($id)
    {
        $model = Banner::findOne($id);
        if ($model->sort != 1) {
            $sort = $model->sort - 1;
            $model_down = Banner::find()->where("sort = $sort")->one();
            $model_down->sort += 1;
            $model_down->save(false);

            $model->sort -= 1;
            $model->save(false);
        }
        $this->redirect(['index']);
    }

    public function actionMoveDown($id)
    {
        $model = Banner::findOne($id);
        $model_max_sort = Banner::find()->orderBy("sort DESC")->one();

        if ($model->id != $model_max_sort->id) {
            $sort = $model->sort + 1;
            $model_up = Banner::find()->where("sort = $sort")->one();
            $model_up->sort -= 1;
            $model_up->save(false);

            $model->sort += 1;
            $model->save(false);
        }
        $this->redirect(['index']);
    }
}
