<?php

namespace app\modules\admin\label;


use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class LabelStatusBanner
{
    public static function statusList()
    {
        return [
            0 => 'Изображение',
            1 => 'Видео',
        ];
    }

    public static function statusLabel($status)
    {
        switch ($status) {
            case 0:
                $class = 'label label-success';
                break;
            case 1:
                $class = 'label label-default';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }


}