<?php

namespace app\modules\admin\label;


use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class LabelStatusButton
{
    public static function statusList()
    {
        return [
            0 => 'Закрыто',
            1 => 'Активно',
        ];
    }

    public static function statusLabel($status)
    {
        switch ($status) {
            case 0:
                $class = 'label label-default';
                break;
            case 1:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }


}