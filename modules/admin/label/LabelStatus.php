<?php

namespace app\modules\admin\label;


use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class LabelStatus
{
    public static function statusList()
    {
        return [
            2 => 'Топ меню',
            1 => 'Активно',
            0 => 'Закрыто',
        ];
    }

    public static function statusLabel($status)
    {
        switch ($status) {
            case 0:
                $class = 'label label-default';
                break;
            case 1:
                $class = 'label label-success';
                break;
            case 2:
                $class = 'label label-default';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }


}