<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EmailRequest */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Эл. почта для связи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-request-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
