<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ListDoctors */

$this->title = 'Создание доктора';
$this->params['breadcrumbs'][] = ['label' => 'Список докторов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="list-doctors-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
