<?php

use yii\helpers\Html;
use app\widgets\MultilingualDetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ListDoctors */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Список докторов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="list-doctors-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= MultilingualDetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'format' => 'raw',
                'attribute' => 'username',
                'value' => function($model){
                    return $model->username;
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'degree',
                'value' => function($model){
                    return $model->degree;
                }
            ],
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($model){
                    return Html::img($model->getImage(), ['width'=>150]);
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'content',
                'value' => function($model){
                    return $model->content;
                }
            ],
        ],
    ]) ?>

</div>
