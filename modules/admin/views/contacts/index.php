<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ContactsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Контакты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contacts-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'format' => 'raw',
                'attribute' => 'address',
                'value' => function($data){
                    return $data->address;
                }
            ],
            'main_phone',
            [
                'format' => 'raw',
                'attribute' => 'phone',
                'value' => function($data){
                    return $data->phone;
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'mode',
                'value' => function($data){
                    return $data->mode;
                }
            ],
            //'iframe:ntext',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}{view}'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
