<?php

use yii\helpers\Html;
use app\widgets\MultilingualDetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Banner */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Баннеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="banner-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= MultilingualDetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            [
                'attribute' => 'status',
                'filter' => \app\modules\admin\label\LabelStatusLanguage::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\label\LabelStatusLanguage::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
            'description:ntext',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($model){
                    return Html::img($model->getImage(), ['width'=>150]);
                }
            ],
            [
                'attribute' => 'statusBanner',
                'filter' => \app\modules\admin\label\LabelStatusBanner::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\label\LabelStatusBanner::statusLabel($model->statusBanner);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'statusButton',
                'filter' => \app\modules\admin\label\LabelStatusButton::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\label\LabelStatusButton::statusLabel($model->statusButton);
                },
                'format' => 'raw',
            ],
            'linkButton',
            'video',
        ],
    ]) ?>

</div>
