<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\SocialImageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Изображение';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="social-image-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>150]);
                }
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update}{image}'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
