<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SocialImage */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Изображение', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="social-image-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
