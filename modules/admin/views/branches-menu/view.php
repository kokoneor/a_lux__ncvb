<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\BranchesMenu */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Отделения меню', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="branches-menu-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'menu_id',
                'value' => function ($model) {
                    return
                        Html::a($model->menu->name, ['/admin/menu/view', 'id' => $model->menu->id]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'branches_id',
                'value' => function ($model) {
                    return
                        Html::a($model->branches->name, ['/admin/branches/view', 'id' => $model->branches->id]);
                },
                'format' => 'raw',
            ],
        ],
    ]) ?>

</div>
