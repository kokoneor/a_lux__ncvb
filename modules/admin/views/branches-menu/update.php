<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BranchesMenu */

$this->title = 'Редактировать Отделения меню: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Отделения меню', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="branches-menu-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
