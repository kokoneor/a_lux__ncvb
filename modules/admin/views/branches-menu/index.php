<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\BranchesMenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отделения меню';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="branches-menu-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'attribute' => 'menu_id',
                'value' => function ($model) {
                    return
                        Html::a($model->menu->name, ['/admin/menu/view', 'id' => $model->menu->id]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'branches_id',
                'value' => function ($model) {
                    return
                        Html::a($model->branches->name, ['/admin/branches/view', 'id' => $model->branches->id]);
                },
                'format' => 'raw',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
