<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BranchesMenu */

$this->title = 'Создание ';
$this->params['breadcrumbs'][] = ['label' => 'Отделения меню', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="branches-menu-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
