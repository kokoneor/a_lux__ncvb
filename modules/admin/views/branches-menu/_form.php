<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BranchesMenu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="branches-menu-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'menu_id')->dropDownList(\app\models\Menu::getList(),
        ['prompt' => '']) ?>

    <?= $form->field($model, 'branches_id')->dropDownList(\app\models\Branches::getList(),
        ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
