<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\SourceMessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Переводы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="source-message-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>
    <?
    $columns = [];

    $columns[] = [
        'class' => 'yii\grid\SerialColumn',
    ];

    $columns[] = [
        'attribute' => 'message',
        'class'     => \app\widgets\GridTextColumn::className(),
        'minWidth'  => 100,
        'maxWidth'  => 200,
    ];

    foreach (\app\models\i18n::locales() as $locale) {
        $columns[] = [
            'attribute' => 'message_'.$locale,
            'class'     => \app\widgets\GridTextColumn::className(),
            'minWidth'  => 100,
            'maxWidth'  => 200,
            'label'     => mb_strtoupper($locale),
            'content'   => function($model) /* @var $model \app\models\SourceMessage */ use ($locale){
                return isset($model->messages[$locale]) ? $model->messages[$locale]->translation : null;
            },
        ];
    }

    $columns[] = [
        'class'     => 'yii\grid\ActionColumn',
        'template'  => '{view}{update}',
    ];
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns,
    ]); ?>

    <?php Pjax::end(); ?>

</div>
