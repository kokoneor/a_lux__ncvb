<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Callback */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Запись к приему', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="callback-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'doctors_id',
                'value' => function ($model) {
                    return
                        Html::a($model->doctors->name, ['/admin/doctors/view', 'id' => $model->doctors->id]);
                },
                'format' => 'raw',
            ],
            'name',
            'phone',
            'date',
        ],
    ]) ?>

</div>
