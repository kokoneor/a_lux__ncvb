<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\MediaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Мы в СМИ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="media-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'format' => 'raw',
                'attribute' => 'title',
                'value' => function($data){
                    return $data->title;
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'link',
                'value' => function($data){
                    return $data->link;
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'content',
                'value' => function($data){
                    return $data->content;
                }
            ],
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>100]);
                }
            ],
            [
                'attribute' => 'status',
                'filter' => \app\modules\admin\label\LabelStatusLanguage::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\label\LabelStatusLanguage::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
            //'metaName',
            //'metaDesc:ntext',
            //'metaKey:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
