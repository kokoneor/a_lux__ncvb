<?php

use yii\helpers\Html;
use app\widgets\MultilingualDetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Menu */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Меню', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="menu-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= MultilingualDetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            ['attribute'=>'parent_id', 'value'=>function($model){ return $model->parentName;}],
            'name',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>200]);
                }
            ],
            [
                'attribute' => 'status',
                'filter' => \app\modules\admin\label\LabelStatus::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\label\LabelStatus::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
            'metaName',
            'metaDesc:ntext',
            'metaKey:ntext',
        ],
    ]) ?>

</div>
