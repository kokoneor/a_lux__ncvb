<?php

use yii\helpers\Html;
use app\widgets\MultilingualDetailView;
use app\models\Feedback;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Отзывы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="feedback-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= MultilingualDetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'surname',
            'patronymic',
            'email',
            'message:ntext',
            'date',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return ArrayHelper::getValue(Feedback::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
        ],
    ]) ?>

</div>
