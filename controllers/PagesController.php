<?php

namespace app\controllers;

use app\models\Page;

class PagesController extends FrontendController
{
    public function actionIndex($id)
    {
        $page = Page::findOne(['id' => $id]);
        $menu = $page->menu;
        $this->setMeta($menu->metaName, $menu->metaDesc, $menu->metaKey);

        return $this->render('index', compact('page'));
    }

    public function actionPage($slug)
    {
        $page = Page::findOne(['slug' => $slug]);
        $menu = $page->menu;
        $this->setMeta($menu->metaName, $menu->metaDesc, $menu->metaKey);

        return $this->render('index', compact('page'));
    }
}