<?php

namespace app\controllers;

use app\models\News;
use app\models\NewsTranslation;
use app\models\Page;
use app\models\PageTranslation;
use yii\data\ActiveDataProvider;

class SearchController extends FrontendController
{
    public function actionIndex($q)
    {
        $this->setMeta('Поиск', "Поиск", "Поиск");

        $queryNews  = NewsTranslation::find()->where(['language' => \Yii::$app->language])
            ->andFilterWhere([
                'or',
                ['like', 'title', $q],
                ['like', 'description', $q],
                ['like', 'content', $q],
                ['like', 'metaName', $q],
                ['like', 'metaKey', $q],
            ])->all();

////        $queryNews  = News::find()->orderBy(['date' => SORT_DESC]);
////        $queryNews->joinWith('newsTranslations');
////        $queryNews
////            ->andFilterWhere([
////                'or',
////                ['like', 'title', $q],
////                ['like', 'description', $q],
////                ['like', 'content', $q],
////                ['like', 'metaName', $q],
////                ['like', 'metaKey', $q],
////            ])->all();
//
//        print_r("<pre>");
//        print_r($queryNews); die;

        $queryPage  = PageTranslation::find()->where(['language' => \Yii::$app->language])
            ->andFilterWhere([
            'or',
            ['like', 'title', $q],
            ['like', 'content', $q],
        ])->all();

        return $this->render('search', compact('q', 'queryNews', 'queryPage'));
    }
}