<?php

namespace app\controllers;

use app\models\LoginForm;
use app\models\User;
use Yii;

class AuthController extends FrontendController
{
    public $layout = 'auth';

    public function actionLogin()
    {
        if(!Yii::$app->user->isGuest){
            return $this->redirect('/admin/menu/index');
        }

        $model = new LoginForm();
        if($model->load(Yii::$app->request->post()) && $model->login()){
            return $this->redirect('/admin/menu/index');
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect('/auth/login');
    }
}