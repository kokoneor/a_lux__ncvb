<?php

namespace app\controllers;

use app\models\Feedback;
use app\models\Menu;
use app\models\search\FeedbackSearch;

class FeedbackController extends FrontendController
{
    public function actionIndex()
    {
        $model              = new Feedback();
        $menu               = Menu::findOne(['name' => 'Главная']);
        $this->setMeta('Отзывы', 'Отзывы');

        $searchModel        = new FeedbackSearch();
        $dataProvider       = $searchModel->searchFront(\Yii::$app->requestedParams);

        return $this->render('index', compact('model', 'menu',
            'searchModel', 'dataProvider'));
    }

    public function actionCreate()
    {
        $model          = new Feedback();
        $model->date    = \Yii::$app->formatter->asDate(time(), "Y-MM-dd");

        if($model->load(\Yii::$app->request->post()) && $model->save()){
            \Yii::$app->session->setFlash('success', \Yii::t('main', 'Спасибо за оставленную отзыв!'));
            return $this->redirect(['index']);
        }
    }
}