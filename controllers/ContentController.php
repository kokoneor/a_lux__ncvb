<?php

namespace app\controllers;

use app\models\search\MediaSearch;

class ContentController extends FrontendController
{
    public function actionMedia()
    {
        $searchModel    = new MediaSearch();
        $dataProvider   = $searchModel->searchFront(\Yii::$app->request->queryParams);

        return $this->render('media', [
            'searchModel'   => $searchModel,
            'dataProvider'  => $dataProvider,
        ]);
    }
}