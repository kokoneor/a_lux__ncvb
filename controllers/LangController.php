<?php

namespace app\controllers;

use yii\web\Controller;
use yii\web\ForbiddenHttpException;

class LangController extends Controller
{
    public function actionIndex($url)
    {
        if($url == 'ru'){
            \Yii::$app->session->set('lang', '');
        }elseif ($url == 'kk'){
            \Yii::$app->session->set('lang', 'kk');
        }elseif ($url == 'en'){
            \Yii::$app->session->set('lang', 'en');
        }else{
            throw new ForbiddenHttpException;
        }

        $this->redirect($_SERVER['HTTP_REFERER']);
    }
}