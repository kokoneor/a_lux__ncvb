<?php

namespace app\controllers;

use app\models\Advantages;
use app\models\Banner;
use app\models\Branches;
use app\models\Callback;
use app\models\Contacts;
use app\models\i18n;
use app\models\Menu;
use app\models\News;
use app\models\Page;
use app\models\Partners;
use app\models\search\NewsSearch;
use app\models\SocialImage;
use app\models\SocialNetwork;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends FrontendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $menu       = Menu::findOne(['name' => 'Главная']);
        $this->setMeta($menu->metaName, $menu->metaDesc, $menu->metaKey);

        $branches   = Branches::find()->all();
        $advantages = Advantages::find()->orderBy('sort ASC')->all();
        $news       = News::find()->orderBy('date DESC')->limit(4)->all();
        $banner     = Banner::find()->orderBy('sort DESC')->all();
        $social     = SocialNetwork::find()->all();
        $contact    = Contacts::find()->orderBy('id ASC')->one();
        $partners   = Partners::find()->all();
        $image      = SocialImage::findOne(['id' => 1]);

        return $this->render('index', compact('news', 'branches', 'advantages', 'banner', 'social',
            'contact', 'partners', 'image'));
    }

    public function actionMakeAnAppointment()
    {
        $model = new Callback();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->sendEmailCallback($model->name, $model->phone, $model->date, $model->doctors->name);
            \Yii::$app->session->setFlash('success', 'Вы успешно записались на прием! Ожидайте звонка!');
            return $this->redirect(['/']);
        }
    }

    public function actionNews()
    {
        $news = News::find()->all();
        $searchModel  = new NewsSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('news', [
            'news'          => $news,
            'searchModel'   => $searchModel,
            'dataProvider'  => $dataProvider,
        ]);
    }

    public function actionMap()
    {
        $model  = Menu::findAll(['status' => 1]);

        return $this->render('map', compact('model'));
    }
    public function actionSearch()
    {
        return $this->render('search');
    }
    public function actionReviews()
    {
        return $this->render('reviews');
    }
//    public function actionSlug()
//    {
//        $pages = Page::find()->all();
//        $news  = News::find()->all();
//
//        foreach($pages as $page){
//            $slug = Page::findOne(['id' => $page->id]);
//            $slug->slug = $this->generateCyrillicToLatin($page->menu->name . '-' . $page->id);
//            $slug->save();
//        }
//
//        foreach($news as $item){
//            $slug_news = News::findOne(['id' => $item->id]);
//            $slug_news->slug = $this->generateCyrillicToLatin(strip_tags($item->title));
//            $slug_news->save();
//        }
//
//        print_r("<h1>Что ты здесь потерял, братишка?!</h1><br><h3>Уходи быстрее отсюда</h3>"); die;
//
//    }
}
