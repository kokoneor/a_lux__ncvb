<?php

namespace app\controllers;

use app\models\ListDoctors;
use app\models\Menu;
use app\models\MultilingualActiveRecord;
use app\models\search\ListDoctorsSearch;
use yii\web\NotFoundHttpException;

class DoctorsController extends FrontendController
{
    /**
     * @inheritdoc
     */
    public function actionIndex()
    {
        $menu           = Menu::findOne(['name' => 'Главная']);
        $searchModel    = new ListDoctorsSearch();
        $dataProvider   = $searchModel->searchFront(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'menu'          => $menu,
            'searchModel'   => $searchModel,
            'dataProvider'  => $dataProvider,
        ]);
    }

    /**
     * Displays a single model.
     * @param $slug
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($slug)
    {
        $model  = $this->findModel($slug);

        return $this->render('view', [
            'model'     => $model
        ]);
    }

    /**
     * Finds the ListDoctors model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $slug
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($slug)
    {
        /* @var $activeRecord MultilingualActiveRecord */

        $model = ListDoctors::find()->where(['slug' => $slug])->one();

        if (null === $model) {
            throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }
}