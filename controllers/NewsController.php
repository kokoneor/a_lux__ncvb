<?php

namespace app\controllers;

use app\models\News;

class NewsController extends FrontendController
{

    public function actionNews($slug)
    {
        $news = News::findOne(['slug' => $slug]);
        $this->setMeta($news->metaName, $news->metaDesc, $news->metaKey);

        return $this->render('internal', compact('news'));
    }

    public function actionInternal($id)
    {
        $news = News::findOne(['id' => $id]);
        $this->setMeta($news->metaName, $news->metaDesc, $news->metaKey);

        return $this->render('internal', compact('news'));
    }
}