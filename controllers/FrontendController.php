<?php

namespace app\controllers;

use app\models\Branches;
use app\models\City;
use app\models\Contacts;
use app\models\EmailRequest;
use app\models\Logo;
use Yii;
use app\models\Menu;
use yii\web\Controller;

class FrontendController extends Controller
{
    public function init()
    {
        $this->setLang();

        $menu       = Menu::findAll(['status' => 1]);
        $top_menu   = Menu::findAll(['status' => 2]);
        $branches   = Branches::find()->all();
        $city       = City::find()->all();
        $logo       = Logo::find()->orderBy('id ASC')->one();
        $contact    = Contacts::find()->orderBy('id ASC')->one();

        Yii::$app->view->params['menu']         = $menu;
        Yii::$app->view->params['top_menu']     = $top_menu;
        Yii::$app->view->params['branches']     = $branches;
        Yii::$app->view->params['city']         = $city;
        Yii::$app->view->params['logo']         = $logo;
        Yii::$app->view->params['contact']      = $contact;
    }

    protected function setMeta($title = null, $description = null, $keywords = null)
    {
        $this->view->title = $title;
        $this->view->registerMetaTag(['name' => 'description', 'content' => "$description"]);
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => "$keywords"]);
    }

    private function setLang()
    {
        if(Yii::$app->session['lang'] == '')
            Yii::$app->language = 'ru';
        elseif(Yii::$app->session['lang'] == 'kk')
            Yii::$app->language = 'kk';
        elseif(Yii::$app->session['lang'] == 'en')
            Yii::$app->language = 'en';
    }

    protected function sendEmailCallback($name, $phone, $date, $doctor)
    {
        $adminEmail = EmailRequest::find()->all();
        $doc        = !empty($doctor) ? '<p>Нужный врач: ' . $doctor . '</p></br>' : '';

        if(!empty($adminEmail)) {
            foreach ($adminEmail as $item) {
                $emailSend = Yii::$app->mailer->compose()
                    ->setFrom([Yii::$app->params['adminEmail'] => 'АО "Научно-исследовательский институт кардиологии и внутренних болезней"'])
                    ->setTo($item->email)
                    ->setSubject('Новая запись к приему врачу')
                    ->setHtmlBody(
                        "<p>Контактное лицо: $name</p></br>
						 <p>Телефон: $phone</p></br>
						 <p>Дата визита: $date</p></br>
						 $doc");

                $emailSend->send();
            }
            return 1;
        }
    }



    public function generateCyrillicToLatin($string, $replacement = '-', $lowercase = true)
    {
        $cyr = [
            'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п',
            'р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
            'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П',
            'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'
        ];
        $lat = [
            'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya',
            'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya'
        ];
        $parts = explode($replacement, str_replace($cyr,$lat, $string));

        $replaced = array_map(function ($element) use ($replacement) {
            $element = preg_replace('/[^a-zA-Z0-9=\s—–-]+/u', '', $element);
            return preg_replace('/[=\s—–-]+/u', $replacement, $element);
        }, $parts);

        $string = trim(implode($replacement, $replaced), $replacement);

        return $lowercase ? strtolower($string) : $string;
    }
}