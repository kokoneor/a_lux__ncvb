<?php

/* @var $this View */
/* @var $searchModel MediaSearch */

/* @var $dataProvider ActiveDataProvider */

use yii\helpers\Html;
use yii\web\View;
use app\models\search\MediaSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

?>

<div class="internal-pages mt-5 mb-5" id="top">
    <button id="top__btn" class="top__btn">
        <a href="#top">Top</a>
    </button>
    <div class="container">
        <? foreach($dataProvider->models as $model): ?>
            <div class="row flex-row-reverse smi-block">
                <div class="row flex-row-reverse">
                    <div class="col-xl-9">
                        <p><strong><?= $model->title; ?></strong></p>

                        <div style="text-align: justify;">
                            <a href="<?= $model->link; ?>" target="_blank">
                                <span style="background-color:transparent; color:rgb(5, 99, 193); font-family:calibri,sans-serif; font-size:11pt">
                                    <?= $model->link; ?>
                                </span>
                            </a>
                        </div>

                        <p style="text-align:justify"><?= $model->content; ?></p>
                    </div>

                    <div class="col-xl-3">
                        <div><img alt="" src="<?= $model->getImage(); ?>" style="height:182px; width:267px"/></div>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
        <div class="row mt-5 mb-5">
            <div class="col-xl-6">
                <?= $this->render('/partials/pagination', [
                    'dataProvider' => $dataProvider,
                ]) ?>
            </div>
        </div>
    </div>
</div>
