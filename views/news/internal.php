<div class="internal mt-5 mb-5">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <h1><?= $news->title; ?></h1>
            </div>
            <div class="col-xl-12">
                <?= $news->content; ?>
            </div>
        </div>
    </div>
</div>