<?php

/* @var $this View */
/* @var $searchModel FeedbackSearch */
/* @var $menu \app\models\Menu */
/* @var $model \app\models\Feedback */

/* @var $dataProvider ActiveDataProvider */

use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use app\models\search\FeedbackSearch;

?>

<div class="container">
    <div class="search-link">
        <ul class="d-flex align-items-center">
            <li><a href="/"><?= $menu->name; ?></a></li>
            <li><img src="/public/dist/image/right-arrow-link.png" alt=""></li>
            <li><a href="#"><?= Yii::t('main', 'Отзывы'); ?></a></li>
        </ul>
    </div>
    <div class="reviews">
        <div class="col-xl-5">
            <?= $this->render('/forms/_form_feedback',[
                'model' => $model,
            ]) ?>
        </div>
        <div class="reviews-title">
            <h1><?= Yii::t('main', 'Отзывы'); ?></h1>
        </div>
        <? foreach($dataProvider->models as $feedback): ?>
            <div class="reviews-block">
                <div class="reviews-block-title">
                    <div class="reviews-name">
                        <span><?= $feedback->uppercase();?></span>
                        <p><?= $feedback->fullName(); ?></p>
                    </div>
                    <div class="reviews-date">
                        <p class="silver-text"><?= $feedback->getDate(); ?></p>
                    </div>
                </div>
                <hr>
                <p><?= $feedback->message; ?></p>
            </div>
        <? endforeach; ?>

        <div class="row mt-5 mb-5">
            <div class="col-xl-6">
                <?= $this->render('/partials/pagination', ['dataProvider' => $dataProvider]) ?>
            </div>
        </div>
    </div>
</div>
