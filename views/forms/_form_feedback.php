<?php

use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="reviews-form">
    <div class="reviews-title">
        <h1><?= Yii::t('main-form', 'Добавить свой отзыв');?></h1>
    </div>
    <? $form = ActiveForm::begin([
        'id'      => 'reviews-form',
        'action'  => ['/feedback/create'],
    ]) ?>
    <label for="">
        <?= Yii::t('main-form', 'Имя');?>
    </label>
    <?= $form->field($model, 'name')->textInput([
        'maxlength'     => true,
        'placeholder'   => Yii::t('main-form', 'Александр'),
        'required'      => 'required'
    ])->label(false) ?>
    <label for="">
        <?= Yii::t('main-form', 'Фамилия');?>
    </label>
    <?= $form->field($model, 'surname')->textInput([
        'maxlength'     => true,
        'placeholder'   => Yii::t('main-form', 'Александров'),
        'required'      => 'required'
    ])->label(false) ?>
    <label for="">
        <?= Yii::t('main-form', 'Отчество');?>
    </label>
    <?= $form->field($model, 'patronymic')->textInput([
        'maxlength'     => true,
        'placeholder'   => Yii::t('main-form', 'Александрович'),
        'required'      => 'required'
    ])->label(false) ?>
    <label for="">
        <?= Yii::t('main-form', 'Электронная почта');?>
    </label>
    <?= $form->field($model, 'email')->textInput([
        'maxlength'     => true,
        'placeholder'   => Yii::t('main-form', 'alexandro@mail.com'),
        'required'      => 'required'
    ])->label(false) ?>
    <label for="">
        <?= Yii::t('main-form', 'Комментарий');?>
    </label>
    <?= $form->field($model, 'message')->textInput([
        'maxlength'     => true,
        'placeholder'   => Yii::t('main-form', 'Введите свой комментарий'),
        'required'      => 'required'
    ])->label(false) ?>

    <?= Html::submitButton(Yii::t('main-form', 'Отправить отзыв'), ['class' => 'btn btn-primary']) ?>

    <? ActiveForm::end() ?>
</div>
