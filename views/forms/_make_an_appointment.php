<?php


use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use app\models\Callback;

/* @var $this yii\web\View */
/* @var $model app\models\Callback */
/* @var $form yii\widgets\ActiveForm */


$model = new Callback();
?>

<? $form = ActiveForm::begin([
    'id'      => 'callback-form',
    'action'  => ['/site/make-an-appointment'],
]) ?>


    <?= $form->field($model, 'name')->textInput([
        'maxlength'   => true,
        'placeholder' => Yii::t('main-form', 'Ваше имя'),
        'required' => 'required'
    ])->label(false) ?>

    <?= $form->field($model, 'phone')->textInput([
        'maxlength'   => true,
        'placeholder' => Yii::t('main-form', 'Номер телефона'),
        'required' => 'required'
    ])->label(false) ?>

    <script>
        $('input[name="Callback[phone]"]').inputmask("8(999) 999-9999");
    </script>

    <div class="doctor-name">
    <?= $form->field($model, 'doctors_id')->dropDownList(\app\models\Doctors::getList(), [
        'prompt' => Yii::t('main', 'Укажите нужного врача'),
        'class'  => 'form-control',
        'required' => 'required'])->label(false) ?>
    </div>

    <div class="calendar">
        <?= $form->field($model, 'date')->textInput([
            'maxlength'                         => true,
            'class'                             => 'datepicker-here',
            'id'                                => 'recipient-name',
            'data-multiple-dates'               => 3,
            'data-multiple-dates-separator'     => ", ",
            'placeholder'                       => Yii::t('main-form', 'Укажите дату визита'),
            'required'                          => 'required'
        ])->label(false) ?>

        <div class="date d-flex align-items-center">
            <span><img src="/public/dist/image/help.png" alt=""></span>
            <p>
                <a href="/doctors"><?= Yii::t('main', 'Посмотреть список всех врачей'); ?></a>
            </p>
        </div>
    </div>
    <?= Html::submitButton(Yii::t('main-form', 'Записаться на прием'), ['class' => 'btn btn-nsb']) ?>

<? ActiveForm::end() ?>