<?php

    /* @var $this View */
    /* @var $searchModel NewsSearch */

    /* @var $dataProvider ActiveDataProvider */

    use yii\data\ActiveDataProvider;
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\web\View;
    use app\models\search\NewsSearch;

?>
<div class="news">
    <div class="container">
        <div class="news-Insitute">
            <div class="title">
                <h1><?= Yii::t('main', 'Новости института');?></h1>
            </div>
            <div class="news-intitute-content">
                <div class="row">
                    <? foreach($dataProvider->models as $item):?>
                        <div class="col-xl-3">
                        <a href="<?= $item->slug ? Url::to(['/news/news', 'slug' => $item->slug]) : Url::to(['/news/internal', 'id' => $item->id]) ?>">
                            <div class="card">
                                <img src="<?= $item->getImage();?>" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <p class="card-text"><small class="text-muted"><?= $item->getDate(); ?></small></p>
                                    <p class="card-text"><b>
                                            <?= $item->title; ?>
                                        </b></p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <? endforeach; ?>
                </div>
                <div class="row mt-5 mb-5">
                    <div class="col-xl-6">
                        <?= $this->render('/partials/pagination', ['dataProvider' => $dataProvider]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>