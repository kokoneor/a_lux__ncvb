<?php

/* @var $this View */
/* @var $model Menu */

use yii\helpers\Html;
use yii\web\View;
use app\models\Menu;
use yii\helpers\Url;

?>

<div class="map">
    <div class="container mt-4 mb-5">
        <div class="row">
            <div class="col-md-3">
                <div class="return">
                    <a href="/"><?= Yii::t('main', 'Главная'); ?></a><span><?= Yii::t('main', 'Карта сайта'); ?></span>
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-12">
                <h1><?= Yii::t('main', 'Карта сайта'); ?></h1>
            </div>
        </div>
        <div class="row mt-5">
            <? foreach($model as $menu): ?>
                <? if($menu->parent == null && !empty($menu->childs)): ?>
                    <div class="col-md-6 mb-5">
                        <div class="map-nav">
                            <h2><?= $menu->name; ?></h2>
                            <ul>
                                <? foreach($menu->childs as $items): ?>
                                    <li>
                                        <a href="<?= $items->page ? !empty($items->page->slug) ? '/page/' . $items->page->slug : '/pages/' . $items->page->id : '#' ?>">
                                            <?= $items->name; ?>
                                        </a>
                                    </li>
                                <? endforeach; ?>
                            </ul>
                        </div>
                    </div>
                <? endif; ?>
            <? endforeach; ?>

            <div class="col-md-6 mb-5">
                <div class="map-nav">
                    <? foreach ($model as $menu): ?>
                        <? if($menu->parent == null && empty($menu->childs)): ?>
                            <h2>
                                <a href="<?= $menu->page ? !empty($menu->page->slug) ? '/page/' . $menu->page->slug : '/pages/' . $menu->page->id : '#' ?>">
                                    <?= $menu->name; ?>
                                </a>
                            </h2>
                        <? endif; ?>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>