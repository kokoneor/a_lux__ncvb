<?php

use yii\helpers\Url;
?>
<div class="main-block" id="top">
    <button id="top__btn" class="top__btn">
        <a href="#top">Top</a>
    </button>
    <div class="container">
        <div class="row">
            <div class="col-xl-7">
                <div class="slider slider-for popup-gallery">
                    <? foreach ($banner as $item) : ?>
                        <? if ($item->status == 1) : ?>
                            <div>
                                <? if ($item->statusBanner == 1) : ?>
                                    <iframe src="https://www.youtube.com/embed/<?= $item->video ?>" style="width: 100%; height: 432px;"></iframe>
                                <? else : ?>
                                    <a href="<?= $item->getImage(); ?>" alt="" class="img-for"><img src="<?= $item->getImage(); ?>" alt=""></a>
                                    <div class="container" style="position: absolute; top: 0; padding-left: 70px; height: 100%;">
                                        <div class="row" style="height: 100%;">
                                            <? if ($item->statusButton == 1) : ?>
                                                <div class="col-xl-6 d-flex justify-content-around flex-column">
                                                    <div>
                                                        <?= !empty($item->title) ? $item->title : '' ?>
                                                        <p><?= !empty($item->description) ? $item->description : '' ?></p>
                                                    </div>
                                                    <a href="<?= $item->linkButton; ?>" class="goToSection"><?= Yii::t('main', 'Перейти к разделу'); ?></a>
                                                </div>
                                            <? endif; ?>
                                        </div>
                                    </div>
                                <? endif; ?>
                            </div>
                        <? endif; ?>
                    <? endforeach; ?>
                </div>
                <div class="slider slider-nav">
                    <? foreach ($banner as $item) : ?>
                        <? if ($item->status == 1) : ?>
                            <? if ($item->statusBanner == 1) : ?>
                                <div>
                                    <iframe src="https://www.youtube.com/embed/<?= $item->video ?>" srcdoc="<style>*
                                        {padding:0;margin:0;overflow:hidden}html,body{height:100%}img,
                                        span{position:absolute;width:100%;top:0;bottom:0;margin:auto}span{height:1.5em;
                                        text-align:center;font:48px/1.5 sans-serif;color:white;text-shadow:0 0 0.5em black}</style>
                                        <a href=<?= $item->video ?>?autoplay=1>
                                        <img src=https://img.youtube.com/vi/<?= $item->video ?>/hqdefault.jpg><span>▶
                                        </span></a>"></iframe></div>
                            <? else : ?>
                                <div>
                                    <img src="<?= $item->getImage(); ?>" alt="" style="width:152px;">
                                </div>
                            <? endif; ?>
                        <? endif; ?>
                    <? endforeach; ?>
                </div>
            </div>
            <div class="col-xl-5">
                <div class="sign-up">
                    <div class="sign-title d-flex align-items-center justify-content-between">
                        <h5><?= Yii::t('main', 'Записаться на прием к врачу'); ?></h5>
                        <img src="/public/dist/image/heart.png" alt="">
                    </div>
                    <p><?= Yii::t('main', 'Вам перезвонит наш сотрудник и уточнит информацию о Вашем визите!'); ?>
                    </p>
                    <?= $this->render('/forms/_make_an_appointment') ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Branches -->
<div class="branches">
    <div class="title">
        <h1><?= Yii::t('main', 'Наши отделения'); ?></h1>
    </div>
    <div class="branches-tab">
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <? $m = 0; ?>
            <? foreach ($branches as $branch) : ?>
                <? $m++; ?>
                <li class="nav-item" id="nav-item-<?= $branch->id; ?>">
                    <a class="nav-link <?= $m == 1 ? 'active' : '' ?>" id="pills-home-tab" data-toggle="pill" href="#pills-home-<?= $branch->id; ?>" role="tab" aria-controls="pills-home-<?= $branch->id; ?>" aria-selected="<?= $m == 1 ? 'true' : '' ?>">
                        <?= $branch->name; ?>
                    </a>
                </li>
            <? endforeach; ?>

        </ul>
        <? $n = 0;
        $array = []; ?>
        <div class="tab-content" id="pills-tabContent">
            <? foreach ($branches as $branch) : ?>
                <? $n++; ?>
                <div class="tab-pane fade <?= $n == 1 ? 'show active' : '' ?>" id="pills-home-<?= $branch->id; ?>" role="tabpanel" aria-labelledby="pills-home-tab">
                    <div class="owl-carousel branches-slider owl-theme" id="owl-<?= $branch->id; ?>">
                        <? foreach ($branch->branchesMenus as $item) : ?>
                            <div class="item">
                                <a href="<?= !empty($item->menu->page->slug) ? Url::to(['/pages/page', 'slug' => $item->menu->page->slug]) : Url::to(['/pages/index', 'id' => $item->menu->id]); ?>">
                                    <img src="<?= $item->menu->getImage(); ?>" alt="">
                                    <div class="slider-text">
                                        <p><?= $item->menu->name; ?></p>
                                    </div>
                                </a>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</div>
<!-- Branches-End -->
<div class="why-are-we">
    <div class="title">
        <p><?= Yii::t('main', 'Почему Вам стоит выбрать именно наш институт??'); ?></p>
    </div>
    <div class="why-are-we-content">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <? foreach ($advantages as $item) : ?>
                    <div class="col-xl-3">
                        <div class="card">
                            <img src="<?= $item->getImage(); ?>" class="card-img-top" alt="...">
                            <div class="card-body">
                                <?= $item->description; ?>
                            </div>
                        </div>
                    </div>
                <? endforeach; ?>

            </div>
        </div>
    </div>
</div>

<!-- News-Insitute -->
<div class="news-Insitute">
    <div class="title">
        <h1><?= Yii::t('main', 'Новости института'); ?></h1>
    </div>
    <div class="news-intitute-content">
        <div class="container">
            <div class="row justify-content-center">
                <? foreach ($news as $item) : ?>
                    <div class="col-xl-3">
                        <a href="<?= $item->slug ? Url::to(['/news/news', 'slug' => $item->slug]) : Url::to(['/news/internal', 'id' => $item->id]) ?>">
                            <div class="card">
                                <img src="<?= $item->getImage(); ?>" class="card-img-top" alt="...">
                                <div class="card-body p-0">
                                    <p class="card-text"><small class="text-muted"><?= $item->getDate(); ?></small></p>
                                    <p class="card-text"><b><?= $item->title; ?></b></p>
                                </div>
                            </div>
                        </a>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
    </div>
    <div class="text-center">
        <a href="/site/news" class="btn btn-nsb"><?= Yii::t('main', 'Читать все новости'); ?></a>
    </div>
</div>
<!-- News-Insitute-End -->

<!-- Why-are-we -->


<!-- Why-are-we-End -->

<!-- Brand-slider -->
<div class="container">
    <div class="brand-slider-main">
        <div class="title">
            <p><?= Yii::t('main', 'Полезные ссылки'); ?></p>
        </div>
        <div class="row justify-content-center">
            <? foreach ($partners as $item) : ?>
                <? if ($item->status == 1) : ?>
                    <div class="col-xl-4 mb-4">
                        <a href="<?= !empty($item->link) ? $item->link : '/' ?>" target="_blank">
                            <div class="item">
                                <img src="<?= $item->getImageAll(); ?>" alt="">
                            </div>
                        </a>
                    </div>
                <? endif; ?>
            <? endforeach; ?>
        </div>
        <!-- <div class="owl-carousel brand-slider owl-theme">
        </div> -->
        <!-- <a href="#" class="brand-prev"><img src="/public/dist/image/left-slider.png" alt=""></a>
        <a href="#" class="brand-next"><img src="/public/dist/image/right-slider.png" alt=""></a> -->
    </div>
    <!-- Brand-slider-End -->


    <!-- Social -->
    <div class="social">
        <div class="social-bg">
            <img src="<?= $image->getImage(); ?>" alt="">
        </div>
        <div class="row">
            <div class="col-xl-9">
                <div class="title text-left">
                    <p><?= Yii::t('main', 'Мы в соцсетях'); ?></p>
                </div>
                <div class="social-text">
                </div>
                <? foreach ($social as $item) : ?>
                    <div class="social-progress">
                        <img src="<?= $item->getImage(); ?>" alt="">
                        <div class="social-account">
                            <p><a href="https://<?= $item->social; ?>" target="_blank"><?= $item->social; ?></a> </p>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
    </div>

    <!-- Social-End -->
</div>

<!-- MAP -->
<div class="map">
    <div class="map-content">
        <div class="title">
            <p><?= Yii::t('main', 'Наши контакты'); ?></p>
        </div>
        <div class="adres">
            <div class="adres-name">
                <p><?= Yii::t('main', 'Адрес'); ?></p>
            </div>
            <h5><?= $contact->address; ?></h5>
            <hr>
        </div>
        <div class="map-contact">
            <div class="telephones mt-4 mb-4">
                <b><?= Yii::t('main', 'Телефоны'); ?></b>
                <div class="row mt-2">
                    <?= $contact->phone; ?>
                </div>
            </div>
            <div class="mode">
                <b><?= Yii::t('main', 'Режим работы'); ?></b>
                <div class="row mt-2">
                    <?= $contact->mode; ?>
                </div>
            </div>
        </div>
    </div>
    <?= $contact->iframe; ?>
</div>

<!-- MAP-END -->