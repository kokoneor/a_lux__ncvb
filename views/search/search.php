<div class="container">
    <div class="search-link">
        <ul class="d-flex align-items-center">
            <li><a href="/"><?= Yii::t('main', 'Главная'); ?></a></li>
            <li><img src="/public/dist/image/right-arrow-link.png" alt=""></li>
            <li><a href="#"><?= Yii::t('main', 'Поиск'); ?></a></li>
        </ul>
    </div>
    <div class="search">
        <div class="search-header">
            <div class="search-title">
                <h1><?= Yii::t('main', 'Результаты по запросу:'); ?></h1>
                <h2><?= $q;?></h2>
            </div>
<!--            <div class="filter-search">-->
<!--                <p>Сортировать по:</p>-->
<!--                <div class="dropdown">-->
<!--                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"-->
<!--                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">-->
<!--                        по пулярности-->
<!--                    </button>-->
<!--                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">-->
<!--                        <a class="dropdown-item" href="#">Action</a>-->
<!--                        <a class="dropdown-item" href="#">Another action</a>-->
<!--                        <a class="dropdown-item" href="#">Something else here</a>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="dropdown">-->
<!--                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"-->
<!--                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">-->
<!--                        по дате-->
<!--                    </button>-->
<!--                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">-->
<!--                        <a class="dropdown-item" href="#">Action</a>-->
<!--                        <a class="dropdown-item" href="#">Another action</a>-->
<!--                        <a class="dropdown-item" href="#">Something else here</a>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
        </div>
        <div class="search-content">
            <div class="search-title">
                <h1><?= Yii::t('main', 'Статьи'); ?></h1>
            </div>
            <div class="row">
                <? foreach($queryNews as $item): ?>
                    <div class="col-xl-3">
                    <div class="card">
                        <img src="<?= $item->id0->getImage() ?>" alt="">
                        <div class="card-text">
                            <div class="silver-text">
                                <p><?= $item->id0->date; ?></p>
                            </div>
                            <p><?= $item->title; ?></p>
                        </div>
                    </div>
                </div>
                <? endforeach; ?>
            </div>
        </div>
        <div class="link-head">
            <div class="search-title">
                <h1><?= Yii::t('main', 'Страницы'); ?></h1>
            </div>
            <div class="link-content">
                <? foreach($queryPage as $item): ?>
                <span><img src="/public/dist/image/red-arrow.png" alt="">
                    <a href="/page/<?= $item->id0->slug; ?>"><?= $item->id0->title ?></a>
                </span>
                <? endforeach; ?>
            </div>
        </div>
    </div>
</div>