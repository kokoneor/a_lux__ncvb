<div class="internal-pages mt-5 mb-5" id="top">
    <button id="top__btn" class="top__btn">
        <a href="#top">Top</a>
    </button>
    <div class="container">
    <h1><?= $page->title; ?></h1>
    
    
    <?= $page->content; ?>
    </div></div>
</div>