<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AdminAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

AdminAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>

<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="<?= Url::toRoute(['menu/index'])?>" class="logo">
            <span class="logo-mini"></span>
            <span class="logo-lg">Админ. Панель</span>
        </a>
<!--        --><?//= Html::a('<span class="logo-mini"></span><span class="logo-lg">Админ. панель</span>', ['default/index'], ['class' => 'logo']) ?>
        <nav class="navbar navbar-static-top">
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown notifications-menu">
                        <!-- Menu toggle button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Заявки">
                            <i class="fa fa-bell-o"></i>
                            <span class="label label-success"><?=count(Yii::$app->view->params['feedback']);?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">У вас <?=count(Yii::$app->view->params['feedback']);?> непрочитанных заявки</li>
                            <li>
                                <ul class="menu">

                                    <? if(Yii::$app->view->params['feedback'] != null):?>
                                        <? foreach (Yii::$app->view->params['feedback'] as $v):?>
                                        <li>
                                            <a href="/admin/feedback/view?id=<?=$v->id;?>">
                                                <i class="fa fa-user text-green"></i> <?=$v->fio;?>
                                            </a>
                                        </li>
                                        <? endforeach;?>
                                    <? endif;?>
                                </ul>
                            </li>
                            <li class="footer"><a href="/admin/feedback/">Посмотреть все заявки</a></li>
                        </ul>
                    </li>
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Админ">
                            <!-- The user image in the navbar-->
                            <img src="/private/dist/img/user.png" class="user-image" alt="User Image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs"><?=Yii::$app->user->identity->username?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="/private/dist/img/user.png" class="img-circle" alt="User Image">

                                <p>
                                    <?=Yii::$app->user->identity->username?> - Administrator
                                </p>
                            </li>

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="/admin/admin-profile/index" class="btn btn-default btn-flat">Профиль</a>
                                </div>
                                <div class="pull-right">
                                    <a href="/auth/logout" class="btn btn-default btn-flat">Выйти</a>
                                </div>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
        </nav>
    </header>

    <aside class="main-sidebar">
        <section class="sidebar">
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="/private/dist/img/user.png" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><?=Yii::$app->user->identity->username?></p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Поиск ...">
                    <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
                </div>
            </form>
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">ОСНОВНАЯ НАВИГАЦИЯ</li>
            </ul>
            <?= dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                    'items' => [
                        ['label' => 'Переводы', 'icon' => 'language', 'url' => ['/admin/source-message/'],'active' => $this->context->id == 'source-message'],
                        ['label' => 'Меню', 'icon' => 'bookmark-o', 'url' => ['/admin/menu/'],'active' => $this->context->id == 'menu'],
                        ['label' => 'Страницы', 'icon' => 'book', 'url' => ['/admin/page/'],'active' => $this->context->id == 'page'],
                        [
                            'label' => 'Главная страница',
                            'icon' => 'book',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Логотип и Копирайт', 'icon' => 'th', 'url' => ['/admin/logo/'],'active' => $this->context->id == 'logo'],
                                ['label' => 'Баннер', 'icon' => 'columns', 'url' => ['/admin/banner/'],'active' => $this->context->id == 'banner'],
                                ['label' => 'Партнеры', 'icon' => 'image', 'url' => ['/admin/partners/'],'active' => $this->context->id == 'partners'],
                                ['label' => 'Наши преимущества', 'icon' => 'suitcase', 'url' => ['/admin/advantages/'],'active' => $this->context->id == 'advantages'],
                                ['label' => 'Врачи', 'icon' => 'suitcase', 'url' => ['/admin/doctors/'],'active' => $this->context->id == 'doctors'],
                                ['label' => 'Наши отделы', 'icon' => 'briefcase', 'url' => ['/admin/branches/'],'active' => $this->context->id == 'branches'],
                                ['label' => 'Отделения', 'icon' => 'briefcase', 'url' => ['/admin/branches-menu/'],'active' => $this->context->id == 'branches-menu/'],
                                ['label' => 'Изображение', 'icon' => 'image', 'url' => ['/admin/social-image/'],'active' => $this->context->id == 'social-image/'],
                            ],
                        ],
                        ['label' => 'Новости', 'icon' => 'calendar', 'url' => ['/admin/news/'],'active' => $this->context->id == 'news'],
                        ['label' => 'Мы в СМИ', 'icon' => 'calendar', 'url' => ['/admin/media/'],'active' => $this->context->id == 'media'],
                        ['label' => 'Список докторов', 'icon' => 'calendar', 'url' => ['/admin/list-doctors/'],'active' => $this->context->id == 'list-doctors'],
                        [
                            'label' => 'Обратная связь',
                            'icon' => 'envelope',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Обратный звонок', 'icon' => 'users', 'url' => ['/admin/callback/'],'active' => $this->context->id == 'callback'],
                                ['label' => 'Отзывы', 'icon' => 'users', 'url' => ['/admin/feedback/'],'active' => $this->context->id == 'feedback'],
                            ],
                        ],
                        [
                            'label' => 'Контакты',
                            'icon' => 'street-view',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Контакты', 'icon' => 'street-view', 'url' => ['/admin/contacts/'],'active' => $this->context->id == 'contacts'],
                                ['label' => 'Города', 'icon' => 'street-view', 'url' => ['/admin/city/'],'active' => $this->context->id == 'city'],
                                ['label' => 'Соц. сети', 'icon' => 'street-view', 'url' => ['/admin/social-network/'],'active' => $this->context->id == 'social-network'],
                                ['label' => 'Эл. почта для связи', 'icon' => 'ship', 'url' => ['/admin/email-request/'],'active' => $this->context->id == 'email-request'],
                            ],
                        ],
                    ],
                ]
            ) ?>
        </section>
    </aside>



    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>

            <?=$content?>

        </section>
    </div>

    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.4.0
        </div>
        <strong>Copyright &copy; 2007-2019 <a href="https://a-lux.kz">A-Lux</a>.</strong>
    </footer>


    <div class="control-sidebar-bg"></div>
</div>

<?php $this->endBody() ?>
<script>
    $(document).ready(function(){
        var editor = CKEDITOR.replaceAll();
        CKFinder.setupCKEditor( editor );
    })
    $.widget.bridge('uibutton', $.ui.button);
</script>
</body>
</html>
<?php $this->endPage() ?>
