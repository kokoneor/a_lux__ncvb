<?php

use yii\helpers\Url;
?>
<header>
    <div class="header-content">
        <nav class="navbar navbar-dark bg-dark p-0 mobile-toggle">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-xl-3">
                    <div class="lang">
                        <div class="dropdown">
                            <div class="langMenu" id="langMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="/public/dist/image/flag.png">
                                <p class="choose_lang">
                                    <?= Yii::$app->session['lang'] != '' ? '' : 'RU' ?>
                                    <?= Yii::$app->session['lang'] != 'kk' ? '' : 'KZ' ?>
                                    <?= Yii::$app->session['lang'] != 'en' ? '' : 'EN' ?>
                                </p>
                            </div>
                            <ul class="dropdown-menu language-button selectpicker" onchange="location = this.options[this.selectedIndex].value;">
                                <li <?= Yii::$app->session['lang'] != 'kk' ? '' : 'selected' ?> value="/lang/index?url=kk"><a class="dropdown-item" href="/lang/index?url=kk">KZ</a></li>
                                <li <?= Yii::$app->session['lang'] != '' ? '' : 'selected' ?> value="/lang/index?url=ru"><a class="dropdown-item" href="/lang/index?url=ru">RU</a></li>
                                <li <?= Yii::$app->session['lang'] != 'en' ? '' : 'selected' ?> value="/lang/index?url=en"><a class="dropdown-item" href="/lang/index?url=en">EN</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-9">
                    <div class="header-nav">
                        <ul class="p-0 m-0 d-flex justify-content-end">
                            <li><img src="/public/dist/image/view.png" alt=""><a href="#" class="view-version"><?= Yii::t('main', 'Версия для слабовидящих'); ?></a></li>
<!--                            <li><a href="/doctors">--><?//= Yii::t('main', 'Наши врачи'); ?><!--</a></li>-->
                            <li><a href="/content/media"><?= Yii::t('main', 'Мы в СМИ'); ?></a></li>
                            <li><a href="/site/map"><?= Yii::t('main', 'Карта сайта'); ?></a></li>
                            <? foreach (Yii::$app->view->params['top_menu'] as $item) : ?>
                                <li><img src="<?= $item->getImage(); ?>" alt="">
                                    <a href="<?= $item->page ? !empty($item->page->slug) ? '/page/' . $item->page->slug : '/pages/' . $item->page->id  : '#' ?>">
                                        <?= $item->name; ?>
                                    </a>
                                </li>
                            <? endforeach; ?>
                            <li><a href="/feedback"><?= Yii::t('main', 'Отзывы и предложения'); ?></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-main">
    <div class="header-mobile collapse" id="navbarToggleExternalContent">
                <ul class="p-0 m-0">
                    <li><img src="/public/dist/image/view.png" alt=""><a href="#" class="view-version"><?= Yii::t('main', 'Версия для слабовидящих'); ?></a></li>
                    <li><a href="/content/media"><?= Yii::t('main', 'Мы в СМИ'); ?></a></li>
                    <? foreach (Yii::$app->view->params['top_menu'] as $item) : ?>
                        <li><img src="<?= $item->getImage(); ?>" alt="">
                            <a href="<?= $item->page ? !empty($item->page->slug) ? '/page/' . $item->page->slug : '/pages/' . $item->page->id  : '#' ?>">
                                <?= $item->name; ?>
                            </a>
                        </li>
                    <? endforeach; ?>
                    <li><a href="/feedback"><?= Yii::t('main', 'Отзывы и предложения'); ?></a></li>
                </ul>
            </div>
        <div class="container">
            <div class="row align-items-center  ">
                <div class="col-xl-4">
                    <div class="header-logo d-flex align-items-center">
                        <a href="/">
                            <img src="<?= Yii::$app->view->params['logo']->getImage(); ?>" width="130" alt="">
                        </a>
                        <div class="logo-text">
                            <p><?= Yii::$app->view->params['logo']->text; ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 p-0">
                    <div class="header-search">
                        <form action="/search" method="get">
                            <input placeholder="<?= Yii::t('main', 'Поиск по названию'); ?>"
                            type="text" name="q">
                            <img src="/public/dist/image/search.png" alt="">
                        </form>
                    </div>
                </div>
                <div class="col-xl-4 pl-5">
                    <div class="call d-flex justify-content-between">
                        <div class="text-right">
                            <h5><img src="/public/dist/image/call.png" alt=""><b><?= Yii::$app->view->params['contact']->main_phone; ?></b></h5>
                            <a href="/feedback"><?= Yii::t('main', 'Оставить отзыв'); ?></a>
                        </div>
                        <div class="loc">
                            <div class="dropdown">
                                <div class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="/public/dist/image/loc.png" alt=""><img src="/public/dist/image/loc-arrow.png" alt=""> </div>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <? foreach (Yii::$app->view->params['city'] as $item) : ?>
                                        <a class="dropdown-item" href="#"><?= $item->name; ?></a>
                                    <? endforeach; ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="category d-flex justify-content-between">
                <? foreach (Yii::$app->view->params['menu'] as $item) : ?>
                    <? if ($item->childs) : ?>
                        <div class="dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?= $item->name ?>
                            </button>
                            <?php $i = 0 ?>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                                <? foreach ($item->childs as $value) : ?>
                                    <? if ($value->branchesMenus) : ?>
                                        <? if ($i == 0) : ?>
                                            <? foreach (Yii::$app->view->params['branches'] as $item) : ?>
                                                <? if ($item->branchesMenus) : ?>
                                                    <a class="dropdown-item" href="/#nav-item-<?= $item->id; ?>"><?= $item->name ?></a>
                                                <? endif; ?>
                                            <? endforeach; ?>
                                        <? endif; ?>
                                    <? else : ?>
                                        <a class="dropdown-item" href="<?= $value->page ? !empty($value->page->slug) ? '/page/' . $value->page->slug : '/pages/' . $value->page->id : '#' ?>">
                                            <?= $value->name ?>
                                        </a>
                                    <? endif; ?>
                                    <? $i = 1; ?>
                                <? endforeach; ?>
                            </div>
                        </div>
                    <? elseif ($item->parent_id == null) : ?>
                        <a href="<?= $item->page ? !empty($item->page->slug) ? '/page/' . $item->page->slug : '/pages/' . $item->page->id : '#' ?>">
                            <?= $item->name; ?>
                        </a>
                    <? endif; ?>

                <? endforeach; ?>

            </div>
        </div>
    </div>
</header>