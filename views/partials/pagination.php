<?php

/* @var $dataProvider ActiveDataProvider */

use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\helpers\Url;
use yii\widgets\LinkPager;

?>

<div class="pagination">
    <?= LinkPager::widget([
        'pagination'           => $dataProvider->pagination,
        'options'              => [
            'class' => 'pagination__wrapper',
        ],
        'linkOptions'          => [
            'class' => '',
        ],
        'pageCssClass'         => 'item',
        'disabledPageCssClass' => 'disabled',
        'prevPageCssClass'     => 'backward',
        'nextPageCssClass'     => 'forward',
        'activePageCssClass'   => 'active',
        'hideOnSinglePage'     => true,
        'prevPageLabel'        => Yii::t('main', 'Назад'),
        'nextPageLabel'        => Yii::t('main', 'Вперед'),
    ]) ?>

</div>
