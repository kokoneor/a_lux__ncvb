<?php

/* @var $this View */
/* @var $searchModel ListDoctorsSearch */
/* @var $menu Menu */

/* @var $dataProvider ActiveDataProvider */

use yii\helpers\Html;
use yii\web\View;
use app\models\search\ListDoctorsSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Menu;

?>

<div class="doctor-list">
    <div class="container">
        <div class="link-page">
            <a href="/"><?= $menu->name; ?></a>
            <span><img src="" alt=""><img src="/public/dist/image/right-arrow.png" alt=""></span>
            <a href="#"><?= Yii::t('main', 'Список врачей'); ?></a>
        </div>
        <div class="title">
            <h1><?= Yii::t('main', 'Список врачей'); ?></h1>
        </div>
        <div class="doc-form d-flex align-items-center">
            <p><?= Yii::t('main', 'ФИО врача:'); ?></p>
            <?php $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
                'options' => [
                    'data-pjax' => 1
                ],
            ]); ?>

                <?= $form->field($searchModel, 'username')->textInput([
                        'placeholder'   => Yii::t('main', 'Введите ФИО'),
                ])->label(false) ?>
                <?= Html::submitButton(Yii::t('main', 'Поиск'), ['class' => 'btn btn-nsb']) ?>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="doc-content">
            <div class="row">
                <? foreach ($dataProvider->models as $model): ?>
                    <div class="col-xl-3">
                        <a href="<?= Url::to(['view', 'slug' => $model->slug]) ?>" class="card-link">
                            <div class="card text-center">
                                <img src="<?= $model->getImage(); ?>" alt="...">
                                <div class="card-body">
                                    <h5 class="card-title">
                                        <?= $model->username; ?>
                                    </h5>
                                    <p class="card-text">
                                        <?= $model->degree; ?>
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div>
                <? endforeach;?>
            </div>
        </div>
    </div>
</div>
