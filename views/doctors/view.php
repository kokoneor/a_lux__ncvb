<?php

/* @var $this View */

/* @var $model ListDoctors */

use app\models\ListDoctors;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

?>

<div class="internal-pages mt-5 mb-5" id="top">
    <button id="top__btn" class="top__btn">
        <a href="#top">Top</a>
    </button>
    <div class="container">
        <h1><?= $model->username; ?></h1>


        <?= $model->content; ?>
    </div></div>
</div>
