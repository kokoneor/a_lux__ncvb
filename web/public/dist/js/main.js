console.log('test-test')


$(window).on('load', function () {
    $('.slider-nav ').css('opacity','1');
    $('.img-for').css('opacity','1')
});
    

$(document).ready(function () {

    $('.view-version').click(function (e) {
        e.preventDefault();
        $('body').toggleClass('body-class').css('a', 'font-size:24px;');
        if (this.innerHTML == "Главная версия") {
            this.innerHTML = "Версия для слабовидящих";
            $(this).css('font-size', '16px')
        } else {
            $(this).css('font-size', '18px')
            this.innerHTML = "Главная версия";
        }
        console.log($(this).text())
    })
    window.onscroll = function () {
        if (window.pageYOffset > 660) {
            document.getElementById('top__btn').classList.add('show');
        }
        else {
            document.getElementById('top__btn').classList.remove('show');
        }
    }
    // $('.popup-gallery').magnificPopup({
    //     type: 'image',
    //     gallery: {
    //         enabled: true
    //     }
    // });
    console.log($('.popup-gallery'));
    initialize_owl($("#owl-1"));
    let tabs = [
        { target: '#nav-item-1', owl: '#owl-1' },
        { target: '#nav-item-3', owl: '#owl-3' },
        { target: '#nav-item-4', owl: '#owl-4' },
        { target: '#nav-item-5', owl: '#owl-5' },
    ];
    $('#owl-1').removeClass('owl-hidden');
    tabs.forEach((tab) => {
        $($(tab.target))
            .on('click', function () {
                initialize_owl($(tab.owl));
            })
    });
    tabs.forEach((tab) => {
        $(`a[href="${tab.target}"]`)
            .on('click', function () {
                initialize_owl($(tab.owl));
            })
    });
    function initialize_owl(el) {
        el.on('initialize.owl.carousel', function (event) {
            el.toggleClass('owl-hidden');
        });
        el.owlCarousel({
            loop: el.children.length >= 3,
            rewind: true,
            margin: 25,
            autoplay: false,
            autoplayTimeout: 3000,
            nav: true,
            dots: false,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 3,
                    stagePadding: 20
                }
            }
        });
    }
});
$(document).ready(function () {
    if ($("div").is(".main-block")) {

    }
    // $('.js-select').select2({
    //     minimumResultsForSearch: Infinity,
    //     placeholder: 'Выбор языка',
    // });
    // if (!$("div").is(".main-block")) {


    // }
    let select = document.getElementById("callback-doctors_id");
    if (select.childElementCount <= 1) {
        select.setAttribute("disabled", "disabled");
        select.firstElementChild.innerText = "Врачей нет";
        select.firstElementChild.setAttribute("selected", "selected");
    }
    else {
        select.firstElementChild.setAttribute("hidden", "true")
    }

    function hashCheck(el) {
        let navItem = $('.nav-item');
        let navLink = $('.nav-link');
        for (let i = 0; i < navItem.length; i++) {
            if ("#" + navItem[i].id == el.hash) {
                navLink[i].click();
            }
        }
    }

    let navItem = $('.nav-item');
    let navLink = $('.nav-link')
    for (let i = 0; i < navItem.length; i++) {
        if (window.location.hash == "#" + navItem[i].id) {
            navLink[i].click();
        }
    }


    let anchor = $('.dropdown-item');
    function anchorAction() {
        for (let i = 0; i < anchor.length; i++) {
            if (anchor[i].hash.startsWith("#nav")) {
                anchor[i].addEventListener('click', function () {
                    hashCheck(anchor[i]);
                });
            }
        }
    }
    anchorAction();
    let size = 70;
    let textContent = $('.card-body p b');

    textContent.each(function () {
        if ($(this).text().length > size) {
            $(this).text($(this).text().slice(0, size) + '...');
        }
    });
    var sliderFor = $('.slider-for');
    var sliderNav = $('.slider-nav');
    sliderFor.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        autoplay: false,
        asNavFor: '.slider-nav'
    });
    sliderNav.slick({
        slidesToShow: 3.99999,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        autoplay: false,
        focusOnSelect: true,
        arrows: false,
        responsive: [
            {
                breakpoint: 460,
                settings: {
                    slidesToShow: 2,
                    arrows: false,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                }
            },
            {
                breakpoint: 1032,
                settings: {
                    arrows: false,
                }
            },
        ]
    });

    $('#my-element').data('datepicker');



    var brand = $('.brand-slider');

    brand.owlCarousel({
        loop: true,
        autoplay: false,
        autoplayTimeout: 3000,
        nav: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 4,
            }
        }
    });
    $('.brand-prev').click(function (e) {
        e.preventDefault();
        brand.trigger('prev.owl.carousel');
    })
    $('.brand-next').click(function (e) {
        e.preventDefault();
        brand.trigger('next.owl.carousel');
    })
});