<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/owl.carousel.min.css">
    <link rel="stylesheet" href="./css/owl.theme.default.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/air-datepicker/2.2.3/css/datepicker.min.css">
    <link rel="stylesheet" href="./css/select2.min.css">
    <link rel="stylesheet" href="./css/main.css">
    <title>Document</title>
</head>

<body>
    <header>
        <div class="header-content">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5">
                        <div class="lang">
                            <img src="./image/flag.png" alt="">
                            <select name="" id="">
                                <option value="" hidden>Выбор языка</option>
                                <option value="">KZ</option>
                                <option value="">RU</option>
                                <option value="">EN</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-7">
                        <div class="header-nav">
                            <ul class="p-0 m-0 d-flex justify-content-between">
                                <li><img src="./image/pencil.png" alt=""><a href="#">Блог председателя</a></li>
                                <li><img src="./image/view.png" alt=""><a href="#">Версия для слабовидящих</a></li>
                                <li><img src="./image/gps.png" alt=""><a href="#">Карта сайта</a></li>
                                <li><img src="./image/smi.png" alt=""><a href="#">Мы в СМИ</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-main">
            <div class="container">
                <div class="row align-items-center  ">
                    <div class="col-xl-4">
                        <div class="header-logo d-flex align-items-center">
                            <img src="./image/logo.png" alt="">
                            <div class="logo-text">
                                <p>Научно-исследовательский
                                    институт кардиологии и
                                    внутренних болезней</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 p-0">
                        <div class="header-search">
                            <form action="">
                                <input placeholder="Поиск по названию">
                                <img src="./image/search.png" alt="">
                            </form>
                        </div>
                    </div>
                    <div class="col-xl-4 pl-5">
                        <div class="call d-flex justify-content-between">
                            <div class="text-right">
                                <h5><img src="./image/call.png" alt=""><b>+7 727 233 - 00 - 61</b></h5>
                                <a href="#">Заказать звонок</a>
                            </div>
                            <div class="loc">
                                <div class="dropdown">
                                    <div class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false"><img src="./image/loc.png" alt=""><img
                                            src="./image/loc-arrow.png" alt=""> </div>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">Алмата</a>
                                        <a class="dropdown-item" href="#">Астана</a>
                                        <a class="dropdown-item" href="#">Караганда</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="category d-flex justify-content-between">
                    <select class="js-select js-states form-control">
                        <option>ОБ ИНСТИТУТЕ</option>
                        <option>ОБ ИНСТИТУТЕ</option>
                        <option>ОБ ИНСТИТУТЕ</option>
                    </select>
                    <select class="js-select js-states form-control">
                        <option>НАШИ ОТДЕЛЕНИЯ</option>
                        <option>НАШИ ОТДЕЛЕНИЯ</option>
                        <option>НАШИ ОТДЕЛЕНИЯ</option>
                    </select>
                    <select class="js-select js-states form-control">
                        <option>НАУКА</option>
                        <option>НАУКА</option>
                        <option>НАУКА</option>
                    </select>
                    <select class="js-select js-states form-control">
                        <option>ГОС. ЗАКУПКИ</option>
                        <option>ГОС. ЗАКУПКИ</option>
                        <option>ГОС. ЗАКУПКИ</option>
                    </select>
                    <a href="#">ОБРАЗОВАНИЕ</a>
                    <a href="#">ЗАКОНОДАТЕЛЬСТВО</a>
                    <a href="#">КОНТАКТЫ</a>
                </div>
            </div>
        </div>
    </header>

    <div class="main-block">
        <div class="container">
            <div class="row">
                <div class="col-xl-8">
                    <div class="main-slider">
                        <div class="owl-carousel owl-carousel-1 owl-theme">
                            <div class="item">
                                <img src="./image/main-slider.png" alt="">
                            </div>
                        </div>
                        <div class="left-arrow">
                            <img src="./image/left-arrow.png" alt="">
                        </div>
                        <div class="right-arrow">
                            <img src="./image/right-arrow.png" alt="">
                        </div>
                    </div>
                    <div class="main-slider-2">
                        <div class="owl-carousel owl-carousel-2 owl-theme">
                            <div class="item">
                                <img src="./image/main-slider-1.png" alt="">
                            </div>
                            <div class="item">
                                <img src="./image/main-slider-2.png" alt="">
                            </div>
                            <div class="item">
                                <img src="./image/main-slider-3.png" alt="">
                            </div>
                            <div class="item">
                                <img src="./image/main-slider-4.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4">
                    <div class="sign-up">
                        <div class="sign-title d-flex align-items-center justify-content-between">
                            <h4>Записаться<br>
                                на прием к врачу</h4>
                            <img src="./image/heart.png" alt="">
                        </div>
                        <p>Вам перезвонит наш сотрудник и уточнит
                            информацию о Вашем визите!</p>
                        <form action="">
                            <input type="text" placeholder="Ваше имя">
                            <input type="text" placeholder="Номер телефона">
                        </form>
                        <div class="doctor-name">
                            <select class="js-select js-states form-control">
                                <option>Укажите нужного врача</option>
                                <option>Укажите нужного врача</option>
                                <option>Укажите нужного врача</option>
                            </select>
                        </div>
                        <div class="calendar">
                            <form action="">
                                <input type="text" placeholder="Укажите дату визита" class="datepicker-here"
                                    data-multiple-dates="3" data-multiple-dates-separator=", " />
                                <img src="./image/calendar.png" alt="">
                            </form>
                            <div class="date d-flex align-items-center">
                                <span><img src="image/help.png" alt=""></span>
                                <p>Выбранная дата недоступна?</p>
                            </div>
                        </div>
                        <button class="btn btn-nsb">Записаться на прием</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">

        <!-- Branches -->
        <div class="branches">
            <div class="title">
                <h1>Наши отделения</h1>
            </div>
            <div class="branches-tab">
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab"
                            aria-controls="pills-home" aria-selected="true">Приемное отделение
                            и консультативно-диагностический
                            центр</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab"
                            aria-controls="pills-profile" aria-selected="false">Кардиология
                            и терапия</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab"
                            aria-controls="pills-contact" aria-selected="false">Хирургия
                            и кардиология</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab"
                            aria-controls="pills-contact" aria-selected="false">Инструментально-лабораторные
                            исследования</a>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                        aria-labelledby="pills-home-tab">
                        <div class="owl-carousel branches-slider owl-theme">
                            <div class="item">
                                <img src="image/heartbeat.png" alt="">
                                <div class="slider-text">
                                    <p>Отделение
                                        рентгенэндоваскулярной
                                        терапии</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                        ...
                    </div>
                    <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                        ...
                    </div>
                </div>
            </div>
        </div>
        <!-- Branches-End -->

        <!-- News-Insitute -->
        <div class="news-Insitute">
            <div class="title">
                <h1>Новости института</h1>
            </div>
            <div class="news-intitute-content">
                <div class="row">
                    <div class="col-xl-3">
                        <div class="card">
                            <img src="./image/news-1.png" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p class="card-text"><small class="text-muted">12.10.2019</small></p>
                                <p class="card-text"><b>С мобильной системой
                                        Lumify врачи могут
                                        принимать клинические...</b></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3">
                        <div class="card">
                            <img src="./image/news-2.png" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p class="card-text"><small class="text-muted">12.10.2019</small></p>
                                <p class="card-text"><b>Действительно ли
                                        холестерин так вреден?</b></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3">
                        <div class="card">
                            <img src="./image/news-3.png" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p class="card-text"><small class="text-muted">12.10.2019</small></p>
                                <p class="card-text"><b>Аритмия:
                                        как нормализовать
                                        сердечный ритм</b></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3">
                        <div class="card">
                            <img src="./image/news-4.png" class="card-img-top" alt="...">
                            <div class="card-body">
                                <p class="card-text"><small class="text-muted">12.10.2019</small></p>
                                <p class="card-text"><b>Гипертония: причины
                                        и лечение повышенного
                                        артериального давления</b></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <a href="#" class="btn btn-nsb">Читать все новости</a>
            </div>
        </div>
        <!-- News-Insitute-End -->
    </div>

    <!-- Why-are-we -->
    <div class="why-are-we">
        <div class="container">
            <div class="title">
                <p>Почему именно мы?</p>
            </div>
            <div class="why-are-we-content">
                <div class="row">
            
                        <div class="card">
                            <img src="./image/icon-1.png" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h6 class="card-title">Колоссальный
                                    опыт</h6>
                                <p class="card-text">С другой стороны сложившаяся структура организации требуют от</p>
                            </div>
                        </div>
            
                        <div class="card">
                            <img src="./image/icon-1.png" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h6 class="card-title">Колоссальный
                                    опыт</h6>
                                <p class="card-text">С другой стороны сложившаяся структура организации требуют от</p>
                            </div>
                        </div>
            
                        <div class="card">
                            <img src="./image/icon-1.png" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h6 class="card-title">Колоссальный
                                    опыт</h6>
                                <p class="card-text">С другой стороны сложившаяся структура организации требуют от</p>
                            </div>
                        </div>

                        <div class="card">
                            <img src="./image/icon-1.png" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h6 class="card-title">Колоссальный
                                    опыт</h6>
                                <p class="card-text">С другой стороны сложившаяся структура организации требуют от</p>
                           </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Why-are-we-End -->

    <!-- Brand-slider -->
    <div class="container">
        <div class="brand-slider-main">
            <div class="owl-carousel brand-slider owl-theme">
                <div class="item">
                    <img src="./image/brand-1.png" alt="">
                </div>
                <div class="item">
                    <img src="./image/brand-2.png" alt="">
                </div>
                <div class="item">
                    <img src="./image/brand-3.png" alt="">
                </div>
                <div class="item">
                    <img src="./image/brand-4.png" alt="">
                </div>
            </div>
            <a href="#" class="brand-prev"><img src="image/left-slider.png" alt=""></a>
            <a href="#" class="brand-next"><img src="image/right-slider.png" alt=""></a>
        </div>
        <!-- Brand-slider-End -->


        <!-- Social -->
        <div class="social">
            <div class="col-xl-6">
                <div class="title text-left">
                    <p>Мы в соцсетях</p>
                </div>
                <div class="social-text">
                    <p>С другой стороны сложившаяся структура организации требуют от нас анализа направлений
                        прогрессивного
                        развития. Идейные </p>
                </div>
                <div class="social-progress">
                    <img src="./image/inst.png" alt="">
                    <div class="social-account">
                        <p>instargam.com/ainfo</p>
                        <img src="./image/right-arrow-social.png" alt="">
                    </div>
                    <p>1689 подписчиков</p>
                </div>
                <div class="social-progress">
                    <img src="./image/vk-icon.png" alt="">
                    <div class="social-account">
                        <p>vk.com/ainfomed</p>
                        <img src="./image/right-arrow-social.png" alt="">
                    </div>
                    <p>1056 подписчиков</p>
                </div>
                <div class="social-progress">
                    <img src="./image/telegram-icon.png" alt="">
                    <div class="social-account">
                        <p>telegram.com/ainfo</p>
                        <img src="./image/right-arrow-social.png" alt="">
                    </div>
                    <p>88 подписчиков</p>
                </div>
            </div>
        </div>

        <!-- Social-End -->
    </div>

    <!-- MAP -->
    <div class="map">
        <div class="map-content">
            <div class="title">
                <p>Наши контакты</p>
            </div>
            <div class="adres">
                <img src="./image/adress.png" alt="">
                <h5>Айтеке би, 120 / Масанчи, 25</h5>
                <hr>
            </div>
            <div class="map-contact">
                <div class="telephones mt-4 mb-4">
                    <b>Телефоны</b>
                    <div class="row mt-2">
                        <div class="col-xl-6">
                            <p> Регистратура</p>
                            <p> +7 727 233 - 00 - 61</p>
                        </div>
                        <div class="col-xl-6">
                            <p>Приемный покой</p>
                            <p> +7 727 233 - 00 - 61</p>
                        </div>
                    </div>
                </div>
                <div class="mode">
                    <b>Режим работы</b>
                    <div class="row mt-2">
                        <div class="col-xl-6">
                            <p>пн — пт:</p>
                            <p>с 8:00 до 17:00</p>
                        </div>
                        <div class="col-xl-6">
                            <p> перерыв</p>
                            <p>с 13:00 до 14:001</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <iframe
            src="https://yandex.ru/map-widget/v1/?um=constructor%3Af0fd8cefa3d29aa86627f72ef84369ae65c47a1d0b33f3c883c21abdede46a7c&amp;source=constructor"
            width="100%" height="615" frameborder="0"></iframe>
    </div>

    <!-- MAP-END -->

    <!-- end-block-->
    <div class="container">
        <div class="end-block d-flex justify-content-between">
            <p>АО „НАУЧНО ИССЛЕДОВАТЕЛЬСКИЙ ИНСТИТУТ КАРДИОЛОГИ И ВНУТРЕННИХ БОЛЕЗЕНЕЙ“ — (с) 2019</p>
            <a href="#">Разработано в <span><img src="./image/logo-alux.png" alt=""></span></a>
        </div>
    </div>

     <!-- end-block-->









    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <script src="./js/owl.carousel.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/air-datepicker/2.2.3/js/datepicker.min.js"></script>
    <script src="js/select2.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>