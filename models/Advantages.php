<?php

namespace app\models;

use omgdef\multilingual\MultilingualBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "advantages".
 *
 * @property int $id
 * @property string $description
 * @property string|null $image
 * @property int $sort
 *
 * @property AdvantagesTranslation[] $advantagesTranslations
 */
class Advantages extends MultilingualActiveRecord
{
    public $path = 'uploads/images/advantages/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'advantages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'required'],
            [['sort'], 'integer'],
            [['image'], 'string', 'max' => 255],
            [['description'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'            => 'ID',
            'description'   => 'Описание',
            'image'         => 'Изображение',
            'sort'          => 'Сортировка',
        ]);
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord){
            $model = Advantages::find()->orderBy('id DESC')->one();
            if(!$model || $this->sort != $model->id){
                $this->sort = $model->sort + 1;
            }
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => AdvantagesTranslation::className(),
                'tableName'     => AdvantagesTranslation::tableName(),
                'attributes'    => ['description'],
            ],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvantagesTranslations()
    {
        return $this->hasMany(AdvantagesTranslation::className(), ['id' => 'id']);
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/advantages/' . $this->image : '';
    }
}
