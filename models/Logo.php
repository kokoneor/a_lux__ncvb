<?php

namespace app\models;

use omgdef\multilingual\MultilingualBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "logo".
 *
 * @property int $id
 * @property string $copyright
 * @property string|null $text
 * @property string|null $image
 *
 * @property LogoTranslation[] $logoTranslations
 */
class Logo extends MultilingualActiveRecord
{
    public $path = "uploads/images/logo/";

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['copyright', 'text'], 'required'],
            [['text'], 'string'],
            [['copyright', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'            => 'ID',
            'copyright'     => 'Копирайт',
            'text'          => 'Текст',
            'image'         => 'Изображение',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => LogoTranslation::className(),
                'tableName'     => LogoTranslation::tableName(),
                'attributes'    => ['copyright', 'text'],
            ],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogoTranslations()
    {
        return $this->hasMany(LogoTranslation::className(), ['id' => 'id']);
    }

    public function getImage()
    {
        return (isset($this->image)) ? '/uploads/images/logo/' . $this->image : '';
    }
}
