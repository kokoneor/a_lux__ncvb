<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contacts_translation".
 *
 * @property int $id
 * @property string $language
 * @property string|null $address
 * @property string|null $phone
 * @property string|null $mode
 *
 * @property Contacts $id0
 */
class ContactsTranslation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contacts_translation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'integer'],
            [['phone', 'mode'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['address'], 'string', 'max' => 255],
            [['id', 'language'], 'unique', 'targetAttribute' => ['id', 'language']],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Contacts::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'address' => 'Address',
            'phone' => 'Phone',
            'mode' => 'Mode',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(Contacts::className(), ['id' => 'id']);
    }
}
