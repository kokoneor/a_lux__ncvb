<?php

namespace app\models;

use omgdef\multilingual\MultilingualBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "doctors".
 *
 * @property int $id
 * @property string $name
 *
 * @property DoctorsTranslation[] $doctorsTranslations
 */
class Doctors extends MultilingualActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'doctors';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'        => 'ID',
            'name'      => 'Название',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => DoctorsTranslation::className(),
                'tableName'     => DoctorsTranslation::tableName(),
                'attributes'    => ['name'],
            ],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoctorsTranslations()
    {
        return $this->hasMany(DoctorsTranslation::className(), ['id' => 'id']);
    }

    public static function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }
}
