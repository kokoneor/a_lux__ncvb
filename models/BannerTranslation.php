<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "banner_translation".
 *
 * @property int $id
 * @property int $status
 * @property string $language
 * @property string|null $title
 * @property string|null $description
 * @property string|null $image
 * @property string|null $video
 *
 * @property Banner $id0
 */
class BannerTranslation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'banner_translation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'integer'],
            [['status'], 'integer'],
            [['description'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['title', 'image', 'video'], 'string', 'max' => 255],
            [['id', 'language'], 'unique', 'targetAttribute' => ['id', 'language']],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Banner::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'language'      => 'Language',
            'title'         => 'Title',
            'description'   => 'Description',
            'image'         => 'image',
            'video'         => 'video',
            'status'        => 'status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(Banner::className(), ['id' => 'id']);
    }
}
