<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "callback".
 *
 * @property int $id
 * @property int $doctors_id
 * @property string $name
 * @property string $phone
 * @property string $date
 *
 * @property Doctors $doctors
 */
class Callback extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'callback';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'date'], 'required'],
            [['doctors_id'], 'integer'],
            [['name', 'phone', 'date'], 'string', 'max' => 255],
            [['doctors_id'], 'exist', 'skipOnError' => true, 'targetClass' => Doctors::className(), 'targetAttribute' => ['doctors_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'doctors_id'    => 'Врач',
            'name'          => 'Имя',
            'phone'         => 'Телефон',
            'date'          => 'Дата',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoctors()
    {
        return $this->hasOne(Doctors::className(), ['id' => 'doctors_id']);
    }
}
