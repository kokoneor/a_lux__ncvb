<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partners_translation".
 *
 * @property int $id
 * @property string $language
 * @property string|null $title
 * @property string|null $image
 * @property string|null $link
 * @property int $status
 *
 * @property Partners $id0
 */
class PartnersTranslation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'partners_translation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id', 'status'], 'integer'],
            [['link'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['title', 'image'], 'string', 'max' => 255],
            [['id', 'language'], 'unique', 'targetAttribute' => ['id', 'language']],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Partners::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'title' => 'Title',
            'image' => 'Image',
            'link' => 'Link',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(Partners::className(), ['id' => 'id']);
    }
}
