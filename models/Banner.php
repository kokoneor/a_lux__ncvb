<?php

namespace app\models;

use omgdef\multilingual\MultilingualBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "banner".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $description
 * @property string|null $image
 * @property string|null $video
 * @property int $statusButton
 * @property int $statusBanner
 * @property string|null $linkButton
 * @property int $sort
 * @property int $status
 *
 * @property BannerTranslation[] $bannerTranslations
 */
class Banner extends MultilingualActiveRecord
{
    public $path = 'uploads/images/banner/';

    public $pathVideo = 'uploads/video/banner/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'banner';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['statusBanner', 'statusButton', 'sort', 'status'], 'integer'],
            [['title', 'image', 'video'], 'string', 'max' => 255],
            [['linkButton'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'            => 'ID',
            'title'         => 'Заголовок',
            'description'   => 'Описание',
            'image'         => 'Изображение',
            'video'         => 'Видео',
            'statusButton'  => 'Статус кнопки',
            'statusBanner'  => 'Статус баннера',
            'linkButton'    => 'Ссылка кнопки',
            'sort'          => 'Сортировка',
            'status'        => 'Статус баннера',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => BannerTranslation::className(),
                'tableName'     => BannerTranslation::tableName(),
                'attributes'    => ['title', 'description', 'image', 'video', 'status'],
            ],
        ]);
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord){
            $model = Banner::find()->orderBy('sort DESC')->one();
            if(!$model || $this->id != $model->id){
                $this->sort = $model->sort + 1;
            }
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBannerTranslations()
    {
        return $this->hasMany(BannerTranslation::className(), ['id' => 'id']);
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/banner/' . $this->image : '';
    }

    public function getVideo()
    {
        return ($this->video) ? '/uploads/video/banner/' . $this->video : '';
    }
}
