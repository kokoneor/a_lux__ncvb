<?php

namespace app\models;

use omgdef\multilingual\MultilingualBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "feedback".
 *
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string $patronymic
 * @property string $email
 * @property string|null $message
 * @property string $date
 * @property integer $status
 *
 * @property FeedbackTranslation[] $feedbackTranslations
 */
class Feedback extends MultilingualActiveRecord
{
    const STATUS_ACTIVE     = 1;
    const STATUS_DISABLED   = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'patronymic', 'email'], 'required'],
            [['message'], 'string'],
            [['status'], 'integer'],
            [['date'], 'safe'],
            [['email'], 'email'],
            [['name', 'surname', 'patronymic', 'email'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'            => 'ID',
            'name'          => 'Имя',
            'surname'       => 'Фамилия',
            'patronymic'    => 'Отчество',
            'email'         => 'Элекронная почта',
            'message'       => 'Комментарий',
            'date'          => 'Дата',
            'status'        => 'Статус',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => FeedbackTranslation::className(),
                'tableName'     => FeedbackTranslation::tableName(),
                'attributes'    => ['name', 'message'],
            ],
        ]);
    }

    /**
     * @return array
     */
    public static function statuses()
    {
        return [
            self::STATUS_DISABLED   => 'disabled',
            self::STATUS_ACTIVE     => 'active',
        ];
    }

    /**
     * @return array
     */
    public static function statusDescription()
    {
        return [
            self::STATUS_DISABLED   => 'закрытый',
            self::STATUS_ACTIVE     => 'активный',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedbackTranslations()
    {
        return $this->hasMany(FeedbackTranslation::className(), ['id' => 'id']);
    }

    public function fullName()
    {
        return $this->surname . ' ' . $this->name . ' ' . $this->patronymic;
    }

    public function getDate()
    {
        return Yii::$app->formatter->asDate($this->date, 'dd.MM.Y');
    }

    public function uppercase()
    {
        return strtoupper(substr($this->name, 0, 1));
    }
}
