<?php

namespace app\models;

use omgdef\multilingual\MultilingualBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "list_doctors".
 *
 * @property int $id
 * @property string $username
 * @property string|null $degree
 * @property string|null $image
 * @property string|null $content
 * @property string|null $slug
 *
 * @property ListDoctorsTranslation[] $listDoctorsTranslations
 */
class ListDoctors extends MultilingualActiveRecord
{
    public $path = "uploads/images/list-doctors/";

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'list_doctors';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            [['degree', 'content'], 'string'],
            [['username', 'image', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'        => 'ID',
            'username'  => 'ФИО',
            'degree'    => 'Должность',
            'image'     => 'Изображение',
            'content'   => 'Содержение',
            'slug'      => 'slug',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => ListDoctorsTranslation::className(),
                'tableName'     => ListDoctorsTranslation::tableName(),
                'attributes'    => ['username', 'degree', 'content'],
            ],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getListDoctorsTranslations()
    {
        return $this->hasMany(ListDoctorsTranslation::className(), ['id' => 'id']);
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/list-doctors/' . $this->image : '/public/dist/image/user.png';
    }
}
