<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "logo_translation".
 *
 * @property int $id
 * @property string $language
 * @property string|null $copyright
 * @property string|null $text
 *
 * @property Logo $id0
 */
class LogoTranslation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logo_translation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'integer'],
            [['text'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['copyright'], 'string', 'max' => 255],
            [['id', 'language'], 'unique', 'targetAttribute' => ['id', 'language']],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Logo::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'copyright' => 'Copyright',
            'text' => 'Text',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(Logo::className(), ['id' => 'id']);
    }
}
