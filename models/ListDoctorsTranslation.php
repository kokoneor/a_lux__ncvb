<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "list_doctors_translation".
 *
 * @property int $id
 * @property string $language
 * @property string|null $username
 * @property string|null $degree
 * @property string|null $content
 *
 * @property ListDoctors $id0
 */
class ListDoctorsTranslation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'list_doctors_translation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'integer'],
            [['degree', 'content'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['username'], 'string', 'max' => 255],
            [['id', 'language'], 'unique', 'targetAttribute' => ['id', 'language']],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => ListDoctors::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'username' => 'Username',
            'degree' => 'Degree',
            'content' => 'Content',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(ListDoctors::className(), ['id' => 'id']);
    }
}
