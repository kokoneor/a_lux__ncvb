<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "social_network".
 *
 * @property int $id
 * @property string $social
 * @property string|null $image
 */
class SocialNetwork extends \yii\db\ActiveRecord
{
    public $path = "uploads/images/social-network/";

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'social_network';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['social'], 'required'],
            [['social', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'social'    => 'Социальная сеть',
            'image'     => 'Изображение',
        ];
    }

    public function getImage()
    {
        return(isset($this->image)) ? '/uploads/images/social-network/' . $this->image : '';
    }
}
