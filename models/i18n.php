<?php

namespace app\models;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;

/**
 * Class i18n
 * @package papalapa\yiistart\modules\i18n\models
 */

class i18n
{
    /**
     * @return array|mixed
     * @throws InvalidConfigException|InvalidParamException
     */

    public static function locales()
    {
        $config = require  Yii::$app->basePath . '/message/config.php';

        if(false === $locales = ArrayHelper::getValue($config, 'languages', false)){
            throw new InvalidParamException("Need to set available locales in 'config.php' file: ['ru','kk'] or ['ru' => 'Ru', 'en' => 'kk']");
        }

        if(array_values($locales) !== $locales){
            $locales = array_keys($locales);
        }

        if(!in_array(Yii::$app->language, $locales)){
            throw new InvalidConfigException(sprintf("Only '%s' languages allowed in 'config.php' of your app, not '%s'.", implode(', ', $locales), Yii::$app->language));
        }

        return $locales;
    }
}