<?php

namespace app\models;

use omgdef\multilingual\MultilingualBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "partners".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $link
 * @property string|null $image
 * @property string|null $image_kk
 * @property string|null $image_en
 *
 * @property PartnersTranslation[] $partnersTranslations
 */
class Partners extends MultilingualActiveRecord
{
    public $path = 'uploads/images/partners/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'partners';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['link'], 'required'],
            [['link'], 'string'],
            [['status'], 'integer'],
            [['title', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'            => 'ID',
            'title'         => 'Заголовок',
            'link'          => 'Ссылка',
            'image'         => 'Изображение',
            'status'        => 'Статус партнера',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => PartnersTranslation::className(),
                'tableName'     => PartnersTranslation::tableName(),
                'attributes'    => ['title', 'image', 'link', 'status'],
            ],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartnersTranslations()
    {
        return $this->hasMany(PartnersTranslation::className(), ['id' => 'id']);
    }

    public function getImageAll()
    {
        if(Yii::$app->language == "kk"){
            return ($this->image_kk) ? '/uploads/images/partners/' . $this->image_kk : '';
        }elseif (Yii::$app->language == "en"){
            return ($this->image_en) ? '/uploads/images/partners/' . $this->image_en : '';
        }else {
            return ($this->image) ? '/uploads/images/partners/' . $this->image : '';
        }
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/partners/' . $this->image : '';
    }

    public function getImageKk()
    {
        return ($this->image_kk) ? '/uploads/images/partners/' . $this->image_kk : '';
    }

    public function getImageEn()
    {
        return ($this->image_en) ? '/uploads/images/partners/' . $this->image_en : '';
    }
}
