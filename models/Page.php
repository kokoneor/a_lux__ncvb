<?php

namespace app\models;

use omgdef\multilingual\MultilingualBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "page".
 *
 * @property int $id
 * @property int $menu_id
 * @property string $title
 * @property string|null $content
 * @property string|null $image
 * @property string|null $slug
 *
 * @property Menu $menu
 * @property PageTranslation[] $pageTranslations
 */
class Page extends MultilingualActiveRecord
{
    public $path = 'uploads/image/pages/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['menu_id', 'title'], 'required'],
            [['menu_id'], 'integer'],
            [['content'], 'string'],
            [['title', 'image'], 'string', 'max' => 255],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['menu_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'        => 'ID',
            'menu_id'   => 'Меню',
            'title'     => 'Заголовок',
            'content'   => 'Содержание',
            'image'     => 'Изображение',
            'slug'      => 'slug',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => PageTranslation::className(),
                'tableName'     => PageTranslation::tableName(),
                'attributes'    => ['title', 'content'],
            ],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageTranslations()
    {
        return $this->hasMany(PageTranslation::className(), ['id' => 'id']);
    }

    public function getImage()
    {
        return(isset($this->image)) ? '/uploads/images/pages/' . $this->image : '';
    }
}
