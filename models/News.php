<?php

namespace app\models;

use omgdef\multilingual\MultilingualBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string $title
 * @property string|null $description
 * @property string|null $content
 * @property string|null $image
 * @property string|null $slug
 * @property string|null $metaName
 * @property string|null $metaDesc
 * @property string|null $metaKey
 * @property string $date
 *
 * @property NewsTranslation[] $newsTranslations
 */
class News extends MultilingualActiveRecord
{
    public $path = "uploads/images/news/";

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'date'], 'required'],
            [['description', 'content', 'slug', 'metaDesc', 'metaKey'], 'string'],
            [['date'], 'safe'],
            [['title', 'image', 'metaName'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'            => 'ID',
            'title'         => 'Заголовок',
            'description'   => 'Краткое описание',
            'content'       => 'Контент',
            'image'         => 'Изображение',
            'metaName'      => 'Мета название',
            'metaDesc'      => 'Мета описание',
            'metaKey'       => 'Мета ключи',
            'date'          => 'Дата',
            'slug'          => 'slug',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => NewsTranslation::className(),
                'tableName'     => NewsTranslation::tableName(),
                'attributes'    => ['title', 'description', 'content', 'metaName', 'metaDesc', 'metaKey'],
            ],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewsTranslations()
    {
        return $this->hasMany(NewsTranslation::className(), ['id' => 'id']);
    }

    public function getImage()
    {
        return (isset($this->image)) ? '/uploads/images/news/' . $this->image : '';
    }

    public function getDate()
    {
        return Yii::$app->formatter->asDate($this->date, 'dd-MM-Y');
    }
}
