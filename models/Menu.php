<?php

namespace app\models;

use omgdef\multilingual\MultilingualBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "menu".
 *
 * @property int $id
 * @property int|null $parent_id
 * @property string $name
 * @property string|null $image
 * @property int $status
 * @property string|null $metaName
 * @property string|null $metaDesc
 * @property string|null $metaKey
 *
 * @property BranchesMenu[] $branchesMenus
 * @property MenuTranslation[] $menuTranslations
 * @property Page[] $pages
 */
class Menu extends MultilingualActiveRecord
{
    public $path = "uploads/images/menu/";

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id', 'status'], 'integer'],
            [['name', 'status'], 'required'],
            [['metaDesc', 'metaKey'], 'string'],
            [['name'], 'string', 'max' => 128],
            [['image', 'metaName'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'            => 'ID',
            'parent_id'     => 'Родительская категория',
            'name'          => 'Наименование',
            'image'         => 'Изображение',
            'status'        => 'Статус',
            'metaName'      => 'Мета название',
            'metaDesc'      => 'Мета описание',
            'metaKey'       => 'Мета ключи',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => MenuTranslation::className(),
                'tableName'     => MenuTranslation::tableName(),
                'attributes'    => ['name', 'metaName', 'metaDesc', 'metaKey'],
            ],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuTranslations()
    {
        return $this->hasMany(MenuTranslation::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPages()
    {
        return $this->hasMany(Page::className(), ['menu_id' => 'id']);
    }

    public function getPage()
    {
        return $this->hasOne(Page::className(), ['menu_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBranchesMenus()
    {
        return $this->hasMany(BranchesMenu::className(), ['menu_id' => 'id']);
    }

    public static function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

    public function getImage()
    {
        return(isset($this->image)) ? '/uploads/images/menu/' . $this->image : '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Menu::className(), ['id' => 'parent_id']);
    }

    public function getParentName()
    {
        return (isset($this->parent))? $this->parent->name:'Не задано';
    }

    public function getChilds()
    {
        return $this->hasMany(Menu::className(), ['parent_id' => 'id']);
    }
}
