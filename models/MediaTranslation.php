<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "media_translation".
 *
 * @property int $id
 * @property int $status
 * @property string $language
 * @property string|null $title
 * @property string|null $content
 * @property string|null $metaName
 * @property string|null $metaDesc
 * @property string|null $metaKey
 *
 * @property Media $id0
 */
class MediaTranslation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'media_translation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'integer'],
            [['status'], 'integer'],
            [['content', 'metaDesc', 'metaKey'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['title', 'metaName'], 'string', 'max' => 255],
            [['id', 'language'], 'unique', 'targetAttribute' => ['id', 'language']],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Media::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'status',
            'language' => 'Language',
            'title' => 'Title',
            'content' => 'Content',
            'metaName' => 'Meta Name',
            'metaDesc' => 'Meta Desc',
            'metaKey' => 'Meta Key',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(Media::className(), ['id' => 'id']);
    }
}
