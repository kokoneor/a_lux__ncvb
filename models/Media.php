<?php

namespace app\models;

use omgdef\multilingual\MultilingualBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "media".
 *
 * @property int $id
 * @property int $status
 * @property string $title
 * @property string|null $link
 * @property string|null $content
 * @property string|null $image
 * @property string|null $metaName
 * @property string|null $metaDesc
 * @property string|null $metaKey
 * @property string|null $date
 *
 * @property MediaTranslation[] $mediaTranslations
 */
class Media extends MultilingualActiveRecord
{
    public $path = "uploads/images/media/";

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'media';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['date'], 'safe'],
            [['link', 'content', 'metaDesc', 'metaKey'], 'string'],
            [['title', 'image', 'metaName'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'            => 'ID',
            'status'        => 'Статус',
            'title'         => 'Заголовок',
            'link'          => 'Ссылка',
            'content'       => 'Содержание',
            'image'         => 'Изображение',
            'metaName'      => 'Мета название',
            'metaDesc'      => 'Мета описание',
            'metaKey'       => 'Мета ключи',
            'date'          => 'Дата',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => MediaTranslation::className(),
                'tableName'     => MediaTranslation::tableName(),
                'attributes'    => ['title', 'content', 'metaName', 'metaDesc', 'metaKey', 'status'],
            ],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMediaTranslations()
    {
        return $this->hasMany(MediaTranslation::className(), ['id' => 'id']);
    }

    public function getImage()
    {
        return (isset($this->image)) ? '/uploads/images/media/' . $this->image : '';
    }
}
