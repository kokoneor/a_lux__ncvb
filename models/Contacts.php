<?php

namespace app\models;

use omgdef\multilingual\MultilingualBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "contacts".
 *
 * @property int $id
 * @property string $address
 * @property string $main_phone
 * @property string|null $phone
 * @property string|null $mode
 * @property string|null $iframe
 *
 * @property ContactsTranslation[] $contactsTranslations
 */
class Contacts extends MultilingualActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contacts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address', 'main_phone'], 'required'],
            [['phone', 'mode', 'iframe'], 'string'],
            [['address', 'main_phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'            => 'ID',
            'address'       => 'Адрес',
            'main_phone'    => 'Основной телефон',
            'phone'         => 'Телефоны',
            'mode'          => 'Режим работы',
            'iframe'        => 'Карта',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => ContactsTranslation::className(),
                'tableName'     => ContactsTranslation::tableName(),
                'attributes'    => ['address', 'phone', 'mode'],
            ],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactsTranslations()
    {
        return $this->hasMany(ContactsTranslation::className(), ['id' => 'id']);
    }
}
