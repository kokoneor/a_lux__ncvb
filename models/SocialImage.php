<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "social_image".
 *
 * @property int $id
 * @property string|null $image
 */
class SocialImage extends \yii\db\ActiveRecord
{
    public $path = 'uploads/images/social-image/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'social_image';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'    => 'ID',
            'image' => 'Изображение',
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/social-image/' . $this->image : '';
    }
}
