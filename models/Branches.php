<?php

namespace app\models;

use omgdef\multilingual\MultilingualBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "branches".
 *
 * @property int $id
 * @property string $name
 *
 * @property BranchesMenu[] $branchesMenus
 * @property BranchesTranslation[] $branchesTranslations
 */
class Branches extends MultilingualActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'branches';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return $this->localizedAttributes([
            'id'    => 'ID',
            'name'  => 'Название',
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'MultilingualBehavior' => [
                'class'         => MultilingualBehavior::className(),
                'langClassName' => BranchesTranslation::className(),
                'tableName'     => BranchesTranslation::tableName(),
                'attributes'    => ['name'],
            ],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBranchesMenus()
    {
        return $this->hasMany(BranchesMenu::className(), ['branches_id' => 'id']);
    }

    public function getBranch()
    {
        return $this->hasOne(BranchesMenu::className(), ['branches_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBranchesTranslations()
    {
        return $this->hasMany(BranchesTranslation::className(), ['id' => 'id']);
    }

    public static function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }
}
