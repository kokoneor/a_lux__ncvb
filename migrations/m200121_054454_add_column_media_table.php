<?php

use yii\db\Migration;

/**
 * Class m200121_054454_add_column_media_table
 */
class m200121_054454_add_column_media_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('media_translation', 'status', $this->integer()->defaultValue(1)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('media_translation', 'status');
    }
}
