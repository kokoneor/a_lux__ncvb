<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%branches_menu}}`.
 */
class m191230_053717_create_branches_menu_table extends Migration
{
    public $table_branches              = 'branches';
    public $table_menu                  = 'menu';
    public $table                       = 'branches_menu';

    /**
     * {@inheritdoc}
     */
//    public function safeUp()
//    {
//        $tableOptions = null;
//        if ($this->db->driverName === 'mysql') {
//            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
//        }
//
//        $this->createTable("{{{$this->table}}}", [
//            'id'        => $this->primaryKey(),
//            'menu_id'     => $this->integer()->notNull(),
//            'branches_id'     => $this->integer()->notNull(),
//        ], $tableOptions);
//
//        $onUpdateConstraint = 'RESTRICT';
//        if ($this->db->driverName === 'sqlsrv') {
//            $onUpdateConstraint = 'NO ACTION';
//        }
//        $this->addForeignKey("fk_{$this->table}_{$this->table_menu}", "{{{$this->table}}}", 'menu_id', "{{{$this->table_menu}}}", 'id', 'CASCADE', $onUpdateConstraint);
//        $this->addForeignKey("fk_{$this->table}_{$this->table_branches}", "{{{$this->table}}}", 'branches_id', "{{{$this->table_branches}}}", 'id', 'CASCADE', $onUpdateConstraint);
//    }
//
//    /**
//     * {@inheritdoc}
//     */
//    public function safeDown()
//    {
//        $this->dropForeignKey("fk_{$this->table}_{$this->table_menu}", "{{{$this->table}}}");
//        $this->dropForeignKey("fk_{$this->table}_{$this->table_branches}", "{{{$this->table}}}");
//        $this->dropTable("{{{$this->table}}}");
//    }
}
