<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%callback}}`.
 */
class m191129_072257_create_callback_table extends Migration
{
    public $table_doctors                   = 'doctors';
    public $table                           = 'callback';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'        => $this->primaryKey(),
            'doctors_id'=> $this->integer()->null(),
            'name'      => $this->string(255)->notNull(),
            'phone'     => $this->string(255)->notNull(),
            'date'      => $this->string(255)->notNull(),
        ], $tableOptions);

        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }
        $this->addForeignKey("fk_{$this->table}_{$this->table_doctors}", "{{{$this->table}}}", 'doctors_id', "{{{$this->table_doctors}}}", 'id', 'CASCADE', $onUpdateConstraint);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->table}_{$this->table_doctors}", "{{{$this->table}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
