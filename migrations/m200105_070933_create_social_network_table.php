<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%social_network}}`.
 */
class m200105_070933_create_social_network_table extends Migration
{
    public $table               = 'social_network';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'        => $this->primaryKey(),
            'social'    => $this->string(255)->notNull(),
            'image'     => $this->string(255)->null(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{{$this->table}}}");
    }
}
