<?php

use yii\db\Migration;

/**
 * Class m200117_102633_add_column_banner_table
 */
class m200117_102633_add_column_banner_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('banner', 'status', $this->integer()->defaultValue(1)->notNull());
        $this->addColumn('banner_translation', 'status', $this->integer()->defaultValue(1)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('banner', 'status');
        $this->dropColumn('banner_translation', 'status');
    }
}
