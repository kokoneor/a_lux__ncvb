<?php

use yii\db\Migration;

/**
 * Class m200218_035508_add_column_list_doctors_table
 */
class m200218_035508_add_column_list_doctors_table extends Migration
{
    /**
    //     * {@inheritdoc}
    //     */
    public function safeUp()
    {
        $this->addColumn('list_doctors', 'slug', $this->string(255)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('list_doctors', 'slug');
    }
}
