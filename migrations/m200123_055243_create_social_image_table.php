<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%social_image}}`.
 */
class m200123_055243_create_social_image_table extends Migration
{
    public $table               = 'social_image';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'        => $this->primaryKey(),
            'image'     => $this->string(255)->null(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{{$this->table}}}");
    }
}
