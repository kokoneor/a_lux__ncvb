<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%branches}}`.
 */
class m191230_053706_create_branches_table extends Migration
{
    public $table               = 'branches';
    public $translationTable    = 'branches_translation';

    /**
     * {@inheritdoc}
     */
//    public function safeUp()
//    {
//        $tableOptions = null;
//        if ($this->db->driverName === 'mysql') {
//            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
//        }
//
//        $this->createTable("{{{$this->table}}}", [
//            'id'        => $this->primaryKey(),
//            'name'     => $this->string(255)->notNull(),
//        ], $tableOptions);
//
//        $this->createTable("{{{$this->translationTable}}}", [
//            'id'            => $this->integer()->notNull(),
//            'language'      => $this->string(16)->notNull(),
//            'name'     => $this->string(255)->notNull(),
//        ], $tableOptions);
//
//        $this->addPrimaryKey("pk_{$this->translationTable}_id_language", "{{{$this->translationTable}}}", ['id', 'language']);
//        $onUpdateConstraint = 'RESTRICT';
//        if ($this->db->driverName === 'sqlsrv') {
//            $onUpdateConstraint = 'NO ACTION';
//        }
//        $this->addForeignKey("fk_{$this->translationTable}_{$this->table}", "{{{$this->translationTable}}}", 'id', "{{{$this->table}}}", 'id', 'CASCADE', $onUpdateConstraint);
//        $this->createIndex("idx_{$this->translationTable}_language", "{{{$this->translationTable}}}", 'language');
//    }
//
//    /**
//     * {@inheritdoc}
//     */
//    public function safeDown()
//    {
//        $this->dropForeignKey("fk_{$this->translationTable}_{$this->table}", "{{{$this->translationTable}}}");
//        $this->dropTable("{{{$this->translationTable}}}");
//        $this->dropTable("{{{$this->table}}}");
//    }
}
