<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%page}}`.
 */
class m191227_125316_create_page_table extends Migration
{
    public $table               = 'page';
    public $translationTable    = 'page_translation';
    public $table_menu          = 'menu';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'        => $this->primaryKey(),
            'menu_id'   => $this->integer()->notNull(),
            'title'     => $this->string(255)->notNull(),
            'content'   => $this->text()->null(),
            'image'     => $this->string(255)->null(),
        ], $tableOptions);

        $this->createTable("{{{$this->translationTable}}}", [
            'id'            => $this->integer()->notNull(),
            'language'      => $this->string(16)->notNull(),
            'title'     => $this->string(255)->notNull(),
            'content'   => $this->text()->null(),
        ], $tableOptions);

        $this->addPrimaryKey("pk_{$this->translationTable}_id_language", "{{{$this->translationTable}}}", ['id', 'language']);
        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }
        $this->addForeignKey("fk_{$this->translationTable}_{$this->table}", "{{{$this->translationTable}}}", 'id', "{{{$this->table}}}", 'id', 'CASCADE', $onUpdateConstraint);
        $this->addForeignKey("fk_{$this->table}_{$this->table_menu}", "{{{$this->table}}}", 'menu_id', "{{{$this->table_menu}}}", 'id', 'CASCADE', $onUpdateConstraint);
        $this->createIndex("idx_{$this->translationTable}_language", "{{{$this->translationTable}}}", 'language');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->translationTable}_{$this->table}", "{{{$this->translationTable}}}");
        $this->dropForeignKey("fk_{$this->table}_{$this->table_menu}", "{{{$this->table}}}");
        $this->dropTable("{{{$this->translationTable}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
